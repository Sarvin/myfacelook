<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EavRecord
 *
 * @author root
 */
class EavRecord extends CActiveRecord
{
    /**
     * Сохранять ли автоматически eav атрибуты
     */
    const EAV_AUTO_SAVE=1;
    /**
     * Подгружать ли eav-атрибуты при выборке
     */
    const EAV_AUTO_LOAD=2;
    /**
     * Удалять ли eav даные при удалении записи
     */
    const EAV_AUTO_DELETE=3;
    /**
     * Подгружать ли eav атрибуты при попытке доступа
     */
    const EAV_LOAD_ON_ACCESS=4;
    /**
     * Состояние того что атрибут был подгружен из бд
     */
    const EAV_ATTRIBUTE_LOADED=1;
    /**
     * Состояние того что атрибут не был подгружен, так как он только создан
     */
    const EAV_ATTRIBUTE_IS_NEW=2;
    /**
     * Префикс eav-атрибутов при выборке атрибутов в одном запросе
     */
    const EAV_PREFIX='eav_';
    /**
     * Префикс eav-атрибута в CDbCriteria
     */
    const EAV_FIELD_PREFIX_TOKEN=':eav_';
    /**
     *
     * @var array() Массив eav-атрибутов
     */
    protected $_eavAttributes=array();
    /**
     *
     * @var CList Eav-Атрибуты для сохранения
     */
    protected $_eavForSave;
    /**
     *
     * @var array() Состояние Eav-атрибутов - подгружен/создан
     */
    protected $_eavState=array();
    /**
     *
     * @var CList Параметры класса
     */
    protected $_options;
    /**
     *
     * @var EavCommandBuilder
     */
    protected $_eavBuilder;

    private static $_eavSchema=array();

    public function  __construct($scenario='insert')
    {
        parent::__construct($scenario);
        $this->_eavForSave=new CList();

        $this->_options=new CList(array(
            self::EAV_AUTO_SAVE,
            self::EAV_AUTO_LOAD,
            self::EAV_AUTO_DELETE,
            self::EAV_LOAD_ON_ACCESS
        ));
    }

	public function populateRecord($attributes,$callAfterFind=true)
    {
        $record=parent::populateRecord($attributes,false);
        $record->setOptions($this->getOptions());
        $record->populateEav($attributes);
        if($callAfterFind)
            $record->afterFind();

        return $record;
    }

	public function populateRecords($data,$callAfterFind=true)
	{
        if($this->_options->contains(self::EAV_AUTO_LOAD))
        {
            $onLoad=true;
            $this->_options->remove(self::EAV_AUTO_LOAD);
        }else $onLoad=false;
        
        $records=array();
        //id для связаных записей
        $relationsIds=array();
		foreach($data as $attributes)
        {
			$record=$this->populateRecord($attributes,!$onLoad&&$callAfterFind);
            $records[$record->Id]=$record;
        }

        if($onLoad)
            $this->populateEavs($records,$callAfterFind);
        
		return $records;
	}

    protected function populateEavs($records,$callAfterFind=true)
    {
        if(!count($records))
            return;
        
        $first=reset($records);
        $forLoad=$first->getEavForLoadList();
        $eavs=$first->loadEavArray(array_keys($records),$forLoad);
        foreach($eavs as $id=>$eav)
        {
            $records[$id]->populateEav($eav,$forLoad);
            if($callAfterFind)
                $records[$id]->afterFind();
        }
    }

    protected function populateEav($data,$attributes=null)
    {
        if(empty($attributes))
        {
            $onlyPopulate=true;
            $attributes=$this->getEavSchema()->getColumnNames();
        }else $onlyPopulate=false;

        foreach($attributes as $column)
        {
            if(array_key_exists(self::EAV_PREFIX.$column, $data))
            {
                $this->__set($column, $data[self::EAV_PREFIX.$column]);
            }elseif(!$onlyPopulate) {
                $this->_eavState[$column]=self::EAV_ATTRIBUTE_IS_NEW;
                $this->__set($column, $this->getEavSchema()->getColumn($column)->defaultValue);
            }
            $this->_eavForSave->remove($column);
        }
    }

    public function __get($attribute)
    {
        $column=$this->getEavSchema()->getColumn($attribute);
        if(isset($column))
        {
            // Если об этом атрибуте пока нет даных, то подгружаем его или
            // инициализируем по умолчанию
            if(!isset($this->_eavState[$attribute]))
            {
                // Если автоподгрузка включена, то подгружаем
                if($this->_options->contains(self::EAV_LOAD_ON_ACCESS))
                {
                    Yii::trace('lazy loading eav '.$attribute);
                    $this->loadEav();
                }
                else {
                // Иначе метим поле как подгруженое, и пишем в него значение по умолчанию
                    $this->__set($attribute,$column->defaultValue);
                    $this->_eavForSave->remove($attribute);
                }
            }
            
            return $this->_eavAttributes[$attribute];
        }
        return parent::__get($attribute);
    }

    public function __set($attribute,$value)
    {
        $column=$this->getEavSchema()->getColumn($attribute);
        if(isset($column))
        {
            // Если этот атрибут не был подгружен, то ставим ему статус как-будто
            // он подгружен, это позволит удалить предыдущую запись об атрибуте
            // из таблицы при сохранении
            if(!isset($this->_eavState[$attribute]))
                $this->_eavState[$attribute]=self::EAV_ATTRIBUTE_LOADED;
            //Если этот атрибут не изменялся, то помечаем его для сохранения
            if(($column->defaultValue!==$value)&&!$this->_eavForSave->contains($attribute))
                $this->_eavForSave->add($attribute);
            //И устанавливаем его
            $this->_eavAttributes[$attribute]=$value;
            
            if(property_exists($this, $attribute))
                $this->$attribute=$value;
            return;
        }
        parent::__set($attribute, $value);
    }

    public function __isset($attribute)
    {
        return isset($this->getEavSchema()->columns[$attribute])||parent::__isset($attribute);
    }

    public function __unset($attribute)
    {
        $column=$this->getEavSchema()->getColumn($attribute);
        if(isset($column))
        {
            $this->_eavAttributes[$attribute]=$column->defaultValue;
            return;
        }
        
        parent::__unset($attribute);

    }

    public function entityAttributes()
    {
        return array();
    }

    public function setOptions()
    {
		if(func_num_args()>0)
		{
			$options=func_get_args();
			if(is_array($options[0]))  // the parameter is given as an array
				$options=$options[0];
            $this->_options->copyFrom($options);
		}
        return $this;
    }

    public function getOptions()
    {
        return $this->_options->toArray();
    }

    public function afterSave()
    {
        parent::afterSave();
        if(!$this->_options->contains(self::EAV_AUTO_SAVE))
            return;

        $this->saveEav();
    }

    public function saveEav()
    {
        // Если нет элементов для сохранения
        foreach($this->getEavSchema()->columnsAsAttribute as $column)
        {
            if(($this->$column!=$this->_eavAttributes[$column])
                    &&(!$this->_eavForSave->contains($column)))
                $this->_eavForSave->add($column);
        }
        $forSave=$this->_eavForSave->toArray();
        
        if(empty($forSave))
            return;
        
        $this->deleteEav($forSave);
        
        foreach($forSave as $column)
        {
            $this->getEavCommandBuilder()->createSaveEavCommand($this->Id, $column, $this->$column)->execute();
            $this->_eavState[$column]=self::EAV_ATTRIBUTE_LOADED;
            $this->_eavForSave->remove($column);
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if(!$this->_options->contains(self::EAV_AUTO_DELETE))
            return;

        $this->deleteEav();
    }

    public function deleteEav($attributes=array())
    {
        $columns=$this->getEavSchema()->getColumnNames();
        //Атрибуты для удаления
        $cleared=array();
        //Если не указаны атрибуты для удаления, то берем все
        if(empty($attributes)||!is_array($attributes))
            $attributes=$columns;

        $attributes=array_values(array_intersect($attributes, $columns));
        // Оставляем только те атрибуты, которые не были только что инициализированы
        // То есть о которых пока нет записи в БД
        foreach($attributes as $column)
        {
            if($this->_eavState[$column]!=self::EAV_ATTRIBUTE_IS_NEW)
                $cleared[]=$column;
        }
        //Если нечего удалять, то выходим
        if(empty($cleared))
            return;

        $this->getEavCommandBuilder()->createDeleteEavCommand($this->Id,$cleared)->execute();
    }
    
    public function afterFind()
    {
        if(!$this->_options->contains(self::EAV_AUTO_LOAD))
            return;

        $this->loadEav();
        
        parent::afterFind();
    }


    protected function getEavForLoadList()
    {
        $eavForLoad=array();
        foreach($this->getEavSchema()->getColumnNames() as $attribute)
        {
            if(!isset($this->_eavState[$attribute]))
            {
                $eavForLoad[]=$attribute;
            }
        }
        return $eavForLoad;
    }

    public function with()
    {
		if(func_num_args()>0)
		{
			$with=func_get_args();
			if(is_array($with[0]))  // the parameter is given as an array
				$with=$with[0];
			return new EavFinder($this,$with);
		}
		else
			return $this;
    }

    public function loadEav()
    {
        $attributes=$this->getEavForLoadList();
        $data=$this->loadEavArray($this->Id, $attributes);
        $this->populateEav($data[$this->Id],$attributes);
    }
    /**
     * Загружает ранее не загруженые eav-атрибуты
     */
    protected function loadEavArray($ids,$attributes)
    {
        if(!is_array($ids))
            $ids=array($ids);
        
        $data=$this->getEavCommandBuilder()->createLoadEavCommand($ids,$attributes)->queryAll();
        $eavs=array();
        foreach($data as $row)
        {
            $column=$this->getEavSchema()->getColumn($row['attr']);
            if(isset($column))
                $eavs[$row['idBoard']][self::EAV_PREFIX.$row['attr']]=$column->afterFind($row[$column->rawName]);
            else ;
            /**
             * @todo Вывести ошибку
             */
        }

        foreach($ids as $id)
        {
            if(!isset($eavs[$id]))
                $eavs[$id]=array();
        }

        return $eavs;
    }

    public function find($condition='',$params=array())
    {
		$criteria=$this->getCommandBuilder()->createCriteria($condition,$params);
        $criteria=$this->getEavCommandBuilder()->prepareCriteria($criteria);
        return parent::find($criteria);
    }

    public function findAll($condition='',$params=array())
    {
		$criteria=$this->getCommandBuilder()->createCriteria($condition,$params);
        $criteria=$this->getEavCommandBuilder()->prepareCriteria($criteria);
        return parent::findAll($criteria);
    }

	public function count($condition='',$params=array())
    {
		$criteria=$this->getCommandBuilder()->createCriteria($condition,$params);
        $criteria=$this->getEavCommandBuilder()->prepareCriteria($criteria);
        return parent::count($criteria);
    }
    /**
     *
     * @return EavSchema Схема eav-столбцов
     */
    public function getEavSchema()
    {
        $className=get_class($this);
        if(!isset(self::$_eavSchema[$className]))
        {
            self::$_eavSchema[$className]=new EavSchema($this);
        }
        return self::$_eavSchema[$className];
    }

    public function getEavCommandBuilder()
    {
        if(!isset($this->_eavBuilder))
        {
            $this->_eavBuilder=new EavCommandBuilder($this->getEavSchema());
        }
        return $this->_eavBuilder;
    }
    //put your code here
}