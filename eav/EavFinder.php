<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EavFinder
 *
 * @author root
 */
class EavFinder extends CComponent
{
    /**
     *
     * @var EavRecord
     */
    protected $_owner;
    protected $_relations=array();

    /**
     *
     * @param EavRecord $owner
     * @param array() $relations
     */

    public function  __construct($owner,$relations)
    {
        if(!$owner instanceof EavRecord)
            throw new CDbException('Передан не eav-класс, пожалуйсто, используйте CActiveFinder');
        
        $this->_owner=$owner;
        foreach($relations as $relation)
            $this->_relations[$relation]=$owner->getActiveRelation($relation);

    }
    
    public function find($condition='',$params=array())
    {
        $record=$this->_owner->find($condition, $params);

        return $record;
    }

    public function findAll($condition='',$params=array())
    {
		$records=$this->_owner->findAll($condition,$params);
        foreach ($this->_relations as $relation)
        {
            $this->loadRelatedRecords($records,$relation);
        }
        return $records;
    }

    public function count()
    {
        
    }
    /**
     *
     * @param <type> $records
     * @param CActiveRelation $relation
     */
    protected function loadRelatedRecords($records,$relation)
    {
        $ids=array();
        $finder=CActiveRecord::model($relation->className);
        $relationType=get_class($relation);
        $isHasMany=false;
        switch($relationType)
        {
            case 'CBelongsToRelation':
                $modelKey=$relation->foreignKey;
                $externalKey=$finder->getTableSchema()->primaryKey;
                break;

            case 'CHasManyRelation':
            case 'CHasOneRelation':
                $modelKey=$this->_owner->getTableSchema()->primaryKey;
                $externalKey=$relation->foreignKey;
                break;
        }

        $recordsWithJoin=array();
        foreach($records as $record)
        {
            $recordsWithJoin[$record->$modelKey][]=$record;
        }

        $criteria=new CDbCriteria();
        $criteria->addInCondition($externalKey, array_keys($recordsWithJoin));
        
        if($relationType=='CHasOneRelation')
            $criteria->group=$externalKey;
        
        if($relationType=='CHasManyRelation')
            $isHasMany=true;
        
        $related=$finder->findAll($criteria);
        foreach($related as $record)
        {
            foreach($recordsWithJoin[$record->$externalKey] as $mainRecord)
                $mainRecord->addRelatedRecord($relation->name,$record,$isHasMany);
        }
        

        return $records;
    }

    //put your code here
}
