<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NumericEavColumn
 *
 * @author root
 */
class CheckBoxListEavColumn extends EavColumnSchema
{
    public $defaultValue=array();
    public $rawName='val_int';
    /**
     *
     * @var CheckBoxList
     */
    private $_list;

    public function setOptions($options)
    {
        $this->_list=new CheckBoxList($options,$this->defaultValue);
    }
    
    public function afterFind($value)
    {
        $this->_list->setSelected($value);
        return array_keys($this->_list->getSelected());
    }

    public function beforeSave($value)
    {
        $this->_list->setSelected($value);
        return parent::beforeSave($this->_list->__toString());
    }
    //put your code here
}
