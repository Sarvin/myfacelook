<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EavSchema
 *
 * @author root
 */
class EavSchema extends CComponent
{
	/**
	 * @var array column metadata of this table. Each array element is a CDbColumnSchema object, indexed by column names.
	 */
	public $columns=array();
    /**
     *
     * @var CList Список Eav-атрибутов, которые указаны как публичные св-ва модели
     */
    public $columnsAsAttribute;
    
    private $_builtInColumns=array(
        'int'=>'NumericEavColumn',
        'float'=>'FloatEavColumn',
        'string'=>'StringEavColumn',
        'text'=>'TextEavColumn',
        'checkboxlist'=>'CheckBoxListEavColumn',
    );

    public function  __construct($model)
    {
        $this->columnsAsAttribute=new CList();
        
        foreach($model->entityAttributes() as $column)
        {
            $this->columns[$column[0]]=$this->createColumn($column);
            if(property_exists($model, $column[0]))
                $this->columnsAsAttribute->add($column[0]);

        }
    }

	/**
	 * Gets the named column metadata.
	 * This is a convenient method for retrieving a named column even if it does not exist.
	 * @param string column name
	 * @return EavColumnSchema metadata of the named column. Null if the named column does not exist.
	 */
	public function getColumn($name)
	{
		return isset($this->columns[$name]) ? $this->columns[$name] : null;
	}

    protected function createColumn($column)
    {
        $className=$type=$column[1];
        if(isset($this->_builtInColumns[$type]))
            $className=$this->_builtInColumns[$type];

        $columnSchema=new $className();
        $columnSchema->type=$type;

        $column=array_slice($column, 2);
        foreach($column as $k=>$v)
            $columnSchema->$k=$v;

        return $columnSchema;
    }

	/**
	 * @return array list of column names
	 */
	public function getColumnNames()
	{
		return array_keys($this->columns);
	}
    //put your code here
}
