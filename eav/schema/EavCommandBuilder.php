<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EavCommandBuilder
 *
 * @author root
 */
class EavCommandBuilder extends CComponent
{
    /**
     * Префикс токена eav-атрибута в псевдоописании выборки
     */
    const EAV_TOKEN_PREFIX=':eav_';
    /**
     * Префикс токена eav-параметра в качестве имени атрибута
     */
    const EAV_PARAM_TOKEN_PREFIX=':e_';
    /**
     *
     * @var EavSchema Схема столбцов
     */
    protected $_schema;
    public function  __construct($schema)
    {
        if(!$schema instanceof EavSchema)
            throw new CHttpException(500,'Неверная схема eav-хранилища');

        $this->_schema=$schema;
    }

    protected function extractEavAttributes($string)
    {
        preg_match_all('~'.preg_quote(self::EAV_TOKEN_PREFIX,'~').'([a-zA-Z]+)~',$string,$regs);
        return $regs[1];
    }

    protected function getEavTableInJoinAlias($attribute)
    {
        return 'teav_'.$attribute;
    }
    /**
     *
     * @param string $attribute
     * @param CDbCriteria $criteria
     */
    protected function applyEavJoin($attribute,$criteria)
    {
        if(!isset($this->_schema->columns[$attribute]))
            throw new CHttpException(500,'Неопознаный eav-атрибут в критерии поиска: "'.$attribute.'"');
        $rawName=$this->_schema->getColumn($attribute)->rawName;
        $joinAlias=$this->getEavTableInJoinAlias($attribute);
        $attrToken=self::EAV_PARAM_TOKEN_PREFIX.$attribute;
        $criteria->join.="\nLEFT JOIN BoardInfo as $joinAlias ON {$criteria->alias}.Id={$joinAlias}.idBoard ".
            "AND {$joinAlias}.attr={$attrToken}";

        $criteria->params[$attrToken]=$attribute;
        return $criteria;
    }

    protected function getRawEavAttributeName($attribute)
    {
        return $this->getEavTableInJoinAlias($attribute).'.'.$this->_schema->getColumn($attribute)->rawName;
    }

    protected function applyEavSelect($matches)
    {
        $attribute=$matches[1];
        return $this->getRawEavAttributeName($attribute).' AS '.EavRecord::EAV_PREFIX.$attribute;
    }

    protected function applyEavCondition($matches)
    {
        return $this->getRawEavAttributeName($matches[1]);
    }
    
    protected function applyEavOrder($matches)
    {
        return $this->getRawEavAttributeName($matches[1]);
    }
    /**
     *
     * @param CDbCriteria $criteria
     */
    public function prepareCriteria($criteria)
    {
        if(empty($criteria->alias))
            $criteria->alias='t';
        if(is_array($criteria->select))
            $criteria->select=implode(',', $criteria->select);

        $eavInSelect=$this->extractEavAttributes($criteria->select);
        $eavInCondition=$this->extractEavAttributes($criteria->condition);
        $eavInOrder=$this->extractEavAttributes($criteria->order);
        
        $eavToJoin=array_unique(array_merge($eavInSelect,$eavInCondition,$eavInOrder));
        foreach($eavToJoin as $attribute)
            $criteria=$this->applyEavJoin($attribute, $criteria);

        $criteria->select=preg_replace_callback('~'.preg_quote(self::EAV_TOKEN_PREFIX,'~').'([a-zA-Z]+)~',
                            array(&$this,'applyEavSelect'), $criteria->select);

        $criteria->condition=preg_replace_callback('~'.preg_quote(self::EAV_TOKEN_PREFIX,'~').'([a-zA-Z]+)~',
                            array(&$this,'applyEavCondition'), $criteria->condition);

        $criteria->order=preg_replace('~`('.preg_quote(self::EAV_TOKEN_PREFIX,'~').'([a-zA-Z]+))`~','\1',$criteria->order);
        $criteria->order=preg_replace_callback('~'.preg_quote(self::EAV_TOKEN_PREFIX,'~').'([a-zA-Z]+)~',
                            array(&$this,'applyEavOrder'), $criteria->order);

        return $criteria;
        
    }

    protected function createEavCriteria($ids,$attributes)
    {
        if(!is_array($ids))
            $ids=array($ids);

        $attributes=array_values(array_intersect($this->_schema->getColumnNames(),$attributes));
        $criteria=new CDbCriteria();
        $criteria->addInCondition('attr', $attributes);
        $criteria->addInCondition('idBoard', $ids);
        return $criteria;
    }
    
    public function createLoadEavCommand($ids,$attributes)
    {
        $criteria=$this->createEavCriteria($ids,$attributes);
        return Yii::app()->getDb()->getCommandBuilder()->createFindCommand('BoardInfo', $criteria);
    }

    public function createSaveEavCommand($idRecord,$column,$value)
    {
        $attributes=array(
            'idBoard'=>$idRecord,
            'attr'=>$column
        );
        $attributes=array_merge($attributes,$this->_schema->getColumn($column)->beforeSave($value));

        return Yii::app()->getDb()->getCommandBuilder()->createInsertCommand('BoardInfo', $attributes);
    }
    /**
     *
     * @param <type> $attributes
     * @param <type> $ids
     * @return CDbCommand
     */
    public function createDeleteEavCommand($ids,$attributes)
    {
        $criteria=$this->createEavCriteria($ids,$attributes);
        return Yii::app()->getDb()->getCommandBuilder()->createDeleteCommand('BoardInfo', $criteria);
    }
    //put your code here
}