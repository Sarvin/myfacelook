$(document).on('scroll',function(){
	var step=$('#step');
	var doc=$(document);
	if (step.offset().top-doc.scrollTop()<1000){
		if (step.data('id') && !step.data('loading')) {
			console.log('step '+$('#step').offset().top,'doc '+doc.scrollTop());
			step.data('loading',true)
			$.ajax({
				url:'/photo/index?step='+step.data('id'),
				data:step,
				dataType:'json',
				success:function(data){
					if (data.step){
						$('.photo-view .items:first').append(data.success);
					}
					$('#step').data('id',data.step);
					if (!data.success)
						step.fadeOut();
					step.data('loading',false);
				}
			})
		}
	}
});