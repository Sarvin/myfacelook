$(function(){
	
	
	
	if ($('.date').length)
	{
		$('.date input').datepicker();
	}
	if ($('#calendar').length)
	{
		//$('.wrap .dt').datepicker();
	}
	
	if ($('select').length)
	{
		$('select').selectBox({
			mobile: true,
    		menuSpeed: 'fast'
		});
	}

	$('body').on('click','[data-index]',function(){
		
		var step=parseInt($(this).data('index'),10);

		$('.record-modal .active').removeClass('active');
		$('.record-modal .steps li').eq(step-1).addClass('active');
		$('.record-modal .step-'+step).addClass('active');

		return false;
	});

	$(document).on('click','.like',function(){
		var $this=$(this)
		$.ajax({
			url:'/master/addLike?id='+$(this).data('articul'),
			type:'GET',
			dataType:'json',
			success:function(data){
				if (data.success!=undefined)
				{
					$this.text(data.success);
				}
			},
		})
		return false;
	});
	
	$('body').on('click','.close',function(){
		$.fancybox.close();
	})

	$('.close-alert').on('click',function(){
		$('.alert-view').removeClass('active');
	});

	$('body').on('click','.record-modal [data-service]',function(e){
		e.preventDefault();
		if ($('.record-modal .active').index()==0){
			$('#id_ms').val($(this).data('service'));
		}
		if ($(this).data('service')!=undefined){
			var	date=new Date().toLocaleDateString();
			$.ajax({
                url:'/master/getTimeView?date='+date+'&'+'id_ms='+$(this).data('service'),
                dataType:'json',
                success:function(data){
                    
                    $('#calendar .items').not('.hide, .hide .items').fadeOut(300,function(){
                        $(this).remove();
                    });
                    $('.record-modal .step-3 #Record_date').val(date);
                    var hide=$('#calendar .overview .hide');
                    for (var i = data.timeTable.length-1; i >=0 ; i--) {
                        
                        var clone=hide.clone(true).insertAfter(hide).removeClass('hide');
                        var id=new Date().valueOf()+""+i;
                        
                        $('label',clone)
                            .attr('for',id);
                        $('span.time',clone).text(data.timeTable[i]);

                        $('input[type="radio"]',clone)
                            .prop('value',data.timeTable[i])
                            .attr('id',id);

                        $('span.price',clone).text(data.price+" руб.");
                    };

                    $('#calendar').next().find('.result.item span').text(data.price+" руб.");
                }
            }).done(function(){
            	setTimeout(function(){
                	$('.record-modal #calendar').tinyscrollbar();	
                },500)
            });
		}

		if ($('.record-modal .active').index()==1){
			if ($('#calendar input:checked').length==0)
			{
				$('.alert-view .view').empty().append('Выберите время сеанса!');
				$('.alert-view').addClass('active');
				setTimeout(function(){
					$('.alert-view').removeClass('active')
				},4000)
				return false;
			} else {
				var dt=$('#Record_date').val().split(" ");
				$('.record-modal .step-3 .date').text(dt[0]);
				$('.record-modal .step-3 .time').text(dt[1])
				$('.record-modal .step-3 span:last').text($('#calendar input:checked').closest('.items').find('.price').text());
			}
		}

		$('.record-modal .active').removeClass('active').next().addClass('active');
		
		return false;
	});
	
	$('[data-record]').on('click',function(){
		initModal($(this).data('record'),1);
		return false;
	});

	$('body').on('change', '#calendar input[type="radio"]',function(){
		$('#Record_date').val(new Date(parseInt($('.step-2 .selected').attr('millis'),10)).toLocaleDateString()+" "+$(this).val());
	});

	function initModal(id,step){

		//$('#scrollbarY').tinyscrollbar();

		$.ajax({
			url:'/master/recordForm',
			type:'GET',
			dataType:'json',
			data:{
				id:id,
				step:step,
			},
			success:function(data){
				$('.client-record').empty().append(data.view);
				
				$('.record-modal .dt').data('val',new Date().toLocaleDateString());

                //$('.record-modal .dt')

                $('.record-modal .dt').calendarPicker({
				    monthNames:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
				    dayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
				    useWheel:true,
				    callbackDelay:100,
				    years:0,
				    months:0,
				    days:4,
				    useWheel:false,
				    disableLastDate:true,
				    callback:function(cal) {
				    	var id=$('#id_ms').val();
				    	var date=cal.currentDate.toLocaleDateString();
                        $('.record-modal .dt').data('val',date);
                        $.ajax({
                            url:'/master/getTimeView?date='+date+'&'+'id_ms='+id,
                            dataType:'json',
                            success:function(data){
                                
                                $('#calendar .items').not('.hide, .hide .items').fadeOut(300,function(){
                                    $(this).remove();
                                });
                                $('.record-modal .step-3 #Record_date').val(date);
                                var hide=$('#calendar .overview .hide');
                                for (var i = data.timeTable.length-1; i >=0 ; i--) {
                                    
                                    var clone=hide.clone(true).insertAfter(hide).removeClass('hide');
                                    var id=new Date().valueOf()+""+i;
                                    
                                    $('label',clone)
                                        .attr('for',id);
                                    $('span.time',clone).text(data.timeTable[i]);

                                    $('input[type="radio"]',clone)
                                        .prop('value',data.timeTable[i])
                                        .attr('id',id);

                                    $('span.price',clone).text(data.price+" руб.");
                                };

                                $('#calendar').next().find('.result.item span').text(data.price+" руб.");

                            }
                        }).done(function(){
                        	setTimeout(function(){
                        		$('.record-modal #calendar').tinyscrollbar();
                        	},500)
                        });
				    }});
				$.fancybox.open([
					{
						href:"#record",
						fitToView:true,
						closeBtn:false,
						padding:0,
						width:755,
					}
				])
			}
		})
		return false;
	}

	$('body').not('.record-modal [data-service]').on('click','[data-service]',function(e){
        initModal($(this).data('service'),2);
        return false;
    });

	$('body').on('click','.carusel .next,.carusel .prev',function(){
		if ($(this).hasClass('next'))
			$.fancybox.next();
		else 
			$.fancybox.prev();
		return false;
	})

	function fancyClosed(){
		$('.record-modal .active').removeClass('active');
		$('.record-modal .step-1,.record-modal .steps li:nth-child(1)').addClass('active');
		$('.alert-view').removeClass('close');
	}
	$('.fancy').fancybox({
		padding:0,
		closeBtn:false,
		afterClose:fancyClosed,
		arrows:false,
		afterShow:function(){
			if (this.href.indexOf('photo')>-1)
			{
				if ($(this.href).data('loaded')==undefined){
					$(this.href).data('loaded',true);
					var id=$(this.href).data('id');
					VK.init({apiId: 4907698, onlyWidgets: true});
		            VK.Widgets.Comments("vk_comments_"+id, {limit: 1,height:100, width: "634", attach: false},"photo_"+id);    
		            
		            VK.Widgets.Like("vk_like_"+id, {type: "button"},'photo_like_'+id);

		            VK.Observer.subscribe('widgets.comments.new_comment', 
	                    function(num,last_comment,date,sign)
	                    {
	                        $.ajax({
	                            url:'/master/noticeComment',
	                            dataType:'json',
	                            data:{
	                            	id:id,
	                            	comment:last_comment
	                            },
	                            type:'post',
	                        });
	                    }
		            );
	        	}
			}
		}
	});

	// $('.more').on('click',function(){
	// 	var $this=$(this);
	// 	var data;
		
	// 	if ($this.data('last')){
	// 		$.ajax({
	// 			url:'/master/loadMore',
	// 			dataType:'json',
	// 			type:'GET',
	// 			data:data,
	// 			success:function(data){
	// 				$this.prev().append(data.items);
	// 				$this.data('last',data.last);
	// 				if (data.last==undefined)
	// 					$this.hide();
	// 			}
	// 		});
	// 	}
	// 	return false;
	// });

	if ($('#term').length){
		$('#term').autocomplete({
			serviceUrl:'/catalog/autoComplite',
			onSelect:function(e){
				$('#search').data('url',e.data);
				
				if (e.data.indexOf('/master/')>-1)
				 	window.location.href=e.data;
				
				$('#search_type').attr('value',e.type);
				$('#search_id').attr('value',e.type==1 ? e.id : undefined);
				
				$('#redirect').attr('value',e.type==1 ? true : undefined);

			},
			onSearchComplete:function(query,suggestions){
				if (suggestions.length==0)
					$('#search').removeData('url');
			}
		})
	}
	var breadcrumbs=$('.breadcrumbs');
	var subscribe=$('header').outerHeight() + $('footer').outerHeight() + breadcrumbs.outerHeight();
	var h= screen.height - subscribe;

	if (screen.height>$('section').height())
	{
		$('section:first').css('min-height',h);
	}

})
