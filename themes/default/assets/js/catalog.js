$(function(){
	var step=$('#step'),
		doc=$(document),
		addAddress=function(){},
		myMap;
	$(document).on('scroll',function(){

		if (doc.scrollTop()>=380){
			$('#map').css({
				top:doc.scrollTop()-345,
			})
		} else {
			$('#map').css({
				top:35,
			})
		}

		if (step.offset().top-doc.scrollTop()<1000){
			if (step.data('id') && !step.data('loading')) {
				step.data('loading',true)
				$.ajax({
					url:'/catalog/index?step='+step.data('id'),
					data:step,
					dataType:'json',
					success:function(data){
						var slice=0;
						if (data.step){
							slice=$('.master-list .infinity .master').length;
							$('.master-list .infinity').append(data.success);
						}
						$('#step').data('id',data.step);
						if (!data.step)
							step.fadeOut();
						step.data('loading',false);
						console.log($('.master-list .infinity .master').slice(slice));
						$.each($('.master-list .infinity .master').slice(slice),function(key,val){
							val=$(val);
							addAddress(val.data('address'),$('.baloon',val).html());
						});
					}
				})
			}
		}
	});

	if ($('[data-address]')){
		ymaps.ready(function (){
			var count=0;
			var myGeocoder = ymaps.geocode('г.'+$('#map').data('city'));
				myGeocoder.then(
				    function (res) {
				        myMap=new ymaps.Map('map',{
							center:res.geoObjects.get(0).geometry.getCoordinates(),
							zoom:12,
						});
				        var myGeoObject = new ymaps.GeoObject({
					        geometry: {
					            type: "Point",// тип геометрии - точка
					            coordinates: res.geoObjects.get(0).geometry.getCoordinates() // координаты точки
					       	},
					    });
						myMap.controls.add('zoomControl', { top: 75, left: 5 });
					   	//myMap.geoObjects.add(myGeoObject);
				    },
				    function (err) {
				        console.log	(err)
				    });

			addAddress=function(address,content){
				var myGeocoder = ymaps.geocode(address);
				myGeocoder.then(
				    function (res) {
						myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {
		                    balloonContent: content,
		                    hintContent:$('.baloon-prof',content).text()+' | '+$('.baloon-address',content).text(),
		                    iconContent:count+=1,
		                }, {
		                    balloonContentSize: 'auto',
		                });
		                myPlacemark.name = "Название";
						myPlacemark.description = "Описание";
					   	myMap.geoObjects.add(myPlacemark);
				    },
				    function (err) {

				    });
			}
			$.each($('[data-address]'),function(key,val){
				val=$(val);
				if (val.data('address')){
					addAddress(val.data('address'),$('.baloon',val).html());
				}
			})
		})
	}
})