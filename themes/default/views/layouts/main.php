<?php

	$cs = Yii::app()->clientScript;
	$cs->registerCssFile($this->getAssetsUrl().'/css/reset.css');
	$cs->registerCssFile($this->getAssetsUrl().'/css/main.css?v=11');
	$cs->registerCssFile($this->getAssetsUrl().'/css/selectBox.css');
	$cs->registerCssFile($this->getAssetsUrl().'/css/owl.carousel.css');
	
	$cs->registerCssFile($this->getAssetsUrl().'/css/fancybox/jquery.fancybox.css');
	$cs->registerCssFile($this->getAssetsUrl().'/css/jquery.autocomplete.css');
	$cs->registerCssFile($this->getAssetsUrl().'/css/fancybox/jquery.fancybox-buttons.css');
	$cs->registerCoreScript('jquery');
	$cs->registerCoreScript('jquery.ui');
	$cs->registerScriptFile($this->getAssetsUrl().'/js/lib/jquery.fancybox.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/lib/jquery.fancybox-buttons.js', CClientScript::POS_END);

	//$cs->registerScriptFile('http://api-maps.yandex.ru/2.0.27/?load=package.standard&lang=ru-RU', CClientScript::POS_HEAD);
	
	$cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js', CClientScript::POS_END);
	$cs->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/selectBox.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/tinyscrollbar.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/autocomplite.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/simpleMenu.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->getAssetsUrl().'/js/main.js?v=11', CClientScript::POS_END);

?><!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $this->title; ?></title>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,300,600,400' rel='stylesheet' type='text/css'>
		<script src="//api-maps.yandex.ru/2.0-stable/?lang=ru-RU&load=package.full" type="text/javascript"></script>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	</head>
	<body>
		<?
			if (Yii::app()->user->isGuest){
				$this->renderPartial('//forms/auth',array('model'=>new AuthForm,'recovery'=>new RecoveryForm));
			}
			
			if (Yii::app()->request->cookies['auth']->value){
				Yii::app()->clientScript->registerScript('show_auth_form','$.fancybox.open([{href:"#auth",padding:0,closeBtn:false}]);');
				unset(Yii::app()->request->cookies['auth']);
			}
		?>
		<div class="client-record">
			
		</div>
		<div class="alert-view">
			<div class="title">
				<p>Сообщение</p>
				<span class="close-alert"></span>
			</div>
			<div class="view">
				
			</div>
		</div>
		<header>
            <div class="top">
                <div class="content items">
                    <a href="/" class="logo item">
                        <img src="/media/logo.png" alt="logo">
                    </a>
                    <div class="menu">
                        <ul id="jsddm">
                            <?
                            	$menu=Menu::model()->findByPk(3);
                            	if ($menu)
                            		echo $menu->render($menu->parentItem);
                            ?>
                        </ul>
                    </div>
                    <div class="actions item">
                    	<?if (!Yii::app()->user->isMaster){?>
                        	<a href="#auth" class="fancy">Войти</a>
                        <?} else {?>
                        	<a href="/master/logout?uri=<?=Yii::app()->request->requestUri?>" class="fancy">Выйти</a>
                        <?}?>
                        <a href="/page/masteram-krasoty" class="active">Мастерам красоты</a>
                    </div>
                </div>
            </div>
            <?if ($this->breadcrumbs){?>
	            <div class="content breadcrumbs">
	                <?
		                $this->widget('zii.widgets.CBreadcrumbs', array(
						    'links'=>$this->breadcrumbs,
						    'separator'=>' / ',
						    'htmlOptions'=>
						    array('class'=>'breadcrumbs fix_width')
						));
					?>
	            </div>
            <?}?>
        </header>

		<section class="<?=$this->bg ? 'bg-def' : '' ?>">
			<?php echo $content;?>
		</section>

		<footer>
            <div class="content">
                <a href="#" class="logo item">
                    © MyFaceLook 2015
                </a>
                <ul class="item">
                    <?
                    	$menu=Menu::model()->findByPk(4);
                    	if ($menu)
                    		echo $menu->render($menu->parentItem);
                    ?>
                </ul>
                <div class="social item">
                	<?if ($insta=Config::getValue('social.instagram')){?>
                    	<a href="<?=$insta?>" class="instagramm"><span></span></a>
                    <?}?>
                    <?if ($vk=Config::getValue('social.vk')){?>
                    	<a href="<?=$vk?>" class="vk"><span></span></a>
                    <?}?>
                    <?if ($insta=Config::getValue('social.fb')){?>
                    	<a href="<?=$fb?>" class="fb"><span></span></a>
                    <?}?>
                </div>
            </div>
        </footer>
	</body>
</html>