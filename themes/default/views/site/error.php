<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
$this->bg=false;
?>



<div class="content" style="text-align:center;">
	<h2 class="caption" style="font-size:30px;text-align:center"></h2>
	<div class="error empty" style="font-size:20px;line-height:20px;text-align:center;padding:100px 0;">
		<p style="font-size:28px;font-weight:bold;margin-bottom:10px;">
			<?if (YII_DEBUG)
					echo CHtml::encode($message);
				else
				echo 'Запрашиваемая страница не найдена';
			?>
		</p>
		<img src="/media/404.jpg">
	</div>
</div>