<form name="sort" action="/catalog">
    <div class="items">
        <div class="item">
            <?=CHtml::textField('Filter[term]',$model->term, array('placeholder'=>'Услуга, профессионал или салон','id'=>'term'));?>
        </div>
        <div class="item">
            <?
                $model->id_city=$this->city->id;
            ?>
            <?=CHtml::dropDownList('Filter[id_city]',$model->id_city,CHtml::listData(City::model()->findAll(),'id','name'), 
                array(  'empty'=>'Выберите город', 
                        'placeholder'=>'Район',
                        'id'=>'city',
                        'ajax' => array(
                            'type'   => 'POST',
                            'url'    => $this->createUrl('site/getArea'),
                            //'update' => '#area',
                            'success'=>'js:function(data){
                                $("#area").html(data);
                                //$("#area").selectBox()
                                //$("#area").selectBox("change");
                            }'  
                        )
                    )
                );
            ?>

        </div>
        <div class="item">
            <?=CHtml::dropDownList('Filter[id_area]',$model->id_area,CHtml::listData(Area::model()->findAll('id_city=:city',array(':city'=>$this->city->id)),'id','name'), array('empty'=>'Выберите район', 'placeholder'=>'Район','id'=>'area'));?>
        </div>
        
        <div class="item">
            <?=CHtml::dropDownList('Filter[time]',$model->time,Filter::getTimeList(),array('empty'=>'Время','class'=>"selectBox"));?>
        </div>
        <div class="item">
            <input type="submit" value="Найти">

        </div>
    </div>
    <div class="split"></div>
    <?=CHtml::dropDownList('Filter[price]',$model->price,Filter::getPriceList(),array('class'=>"selectBox"));?>

    <?=CHtml::dropDownList('Filter[gender]',$model->gender,Filter::getGenderList(),array('class'=>"selectBox"));?>

    <?
        foreach (Filter::getStatusList() as $key => $value) {
            ?>
                <input type="checkbox" value="<?=$key?>" <?=$model->status[$key]!=null ? 'checked' : ''?> name="Filter[status][<?=$key?>]" id="<?=$key?>">
                <label for="<?=$key?>">
                    <?=$value?>
                </label>
            <?      
        }
    ?>
    <div class="item">
        <?=CHtml::hiddenField('Filter[search_type]',$model->search_type,array('id'=>'search_type'));?>
    </div>
    <div class="item">
        <?=CHtml::hiddenField('Filter[search_id]',$model->search_id,array('id'=>'search_id'));?>
    </div>
</form>
