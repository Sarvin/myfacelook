
<div class="bg-filter">
    <div class="content">
        <h1 class="title">
            Находи  и  <span class="azure">записывайся</span> на  услуги <span class="azure">лучших</span>  мастеров  своего  города
        </h1>

        <div class="filter bg-fff">
            <?
                $this->renderPartial('_filter',array('model'=>$filter));

            ?>
        </div>
        <div class="service">
            <div class="items">
                <?
                    foreach ($menu->parentItem as $key => $data) {
                    ?>
                        <div class="item">
                            <p><?=$data->name?></p>
                            <ul>
                                <?

                                    foreach ($data->childs as $key => $item) {
                                        ?>
                                            <li>
                                                <a href="<?=$item->getUrl()?>"><?=$item->name?></a>
                                            </li>
                                        <?
                                    }
                                ?>
                            </ul>
                        </div>
                    <?
                    }
                ?>

                <div class="item">
                    <ul>
                        <li>
                            &nbsp;
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
</div>
<div class="masters">
    <div class="content">
        <p class="caption">Мастера</p>
        <div class="items">
            <?
                foreach ($masters as $key => $data) {
                    $this->renderPartial('_master',array('data'=>$data));
                }
            ?>
        </div>
    </div>
</div>
<?if (Yii::app()->user->isGuest){?>
<div class="reg">
    <div class="content">
        <p>
            Вы парикмахер, стилист или визажист? Создайте свое портфолио всего за 5 минут абсолютно бесплатно и получайте заказы  на <a href="#">My facelook</a>
        </p>
        <a href="/master/registration" class="btn-azure">Зарегистрироваться</a>
    </div>
</div>
<?}?>
<div class="example">
    <?=$page->wswg_body?>
</div>
