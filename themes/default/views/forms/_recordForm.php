<?
    $serviceList=$model->getServiceRecordList();
    Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/jquery.calendarPicker.js',CClientScript::POS_END);
?>
<div class="modal record-modal" style="display:none;" id="record">
    <div class="steps">
        <ul>
            <li <?=$step==1 ? ' class="active"': ''?>>
                1. Услуга
            </li>
            <li <?=$step==2 ? ' class="active"': ''?>>
                2. Дата и время
            </li>
            <li <?=$step==3 ? ' class="active"': ''?>>
                3. Контакты
            </li>
        </ul>
        <a href="#" class="close"></a>
    </div>
    <div class="main">
        <div class="step-1 <?=$step==1 ? 'active' : ''?>" data-step="1">
            <div id="scrollbarY">
                <div class="scrollbar" style="height: 424px;">
                    <div class="track" style="height: 424px;">
                        <div class="thumb">
                            <div class="end"></div>
                        </div>
                    </div>
                </div>
                <div class="viewport">
                    <div class="overview" style="top: 0px;">
                        <div class="master-record">
                            <div class="row items">
                                <?foreach ($serviceList as $key => $data) {
                                    ?><p><?=$key?></p><?
                                    $this->renderPartial('//master/_record',array('data'=>$data,'parentName'=>$key));
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="step-2 <?=$step==2 ? 'active' : ''?>" data-step="2">
            <a href="#" class="return" data-index="1"></a>
            <div class="wrap">
                <div class="dt"></div>
                <div id="calendar">
                    <div class="scrollbar" style="height: 260px;">
                        <div class="track" style="height: 260px;">
                            <div class="thumb" style="top: 0px; height: 135.470941883768px;">
                                <div class="end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="viewport">
                        <div class="overview">
                          <div class="items hide">
                                <input type="radio" name="Record[time]">
                                <label for="xx" class="items" style="width: 100%;">
                                    <span class="time"></span>
                                    <span class="price item"></span>
                                </label>
                            </div>
                          <?
                                foreach ($model->getFreeRecordTime(date('Y-m-d'),$id) as $key => $value) {
                                ?>
                                    <div class="items">
                                        <input type="radio" id="xx_<?=$key?>" name="Record[time]" value="<?=$value?>">
                                        <label for="xx_<?=$key?>" class="items">
                                            <span class="time"><?=$value?></span>
                                            <span class="price item"></span>
                                        </label>
                                    </div>
                                <?      
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="items">
                    <span class="result item">
                        Итого:<span>750 руб.</span>
                    </span>
                    <a href="#" class="btn-azure" data-service>
                        Записаться
                    </a>
                </div>
            </div>
        </div>
        <div class="step-3 <?=$step==3 ? 'active' : ''?>" data-step="3">
            <a href="#" class="return" data-index="2"></a>
            <p class="caption">
                Стрижка у  мастера <?=$model->fio?> <span class="date"></span> на <span class="time">12:15.</span> Стоимость услуги <span>750</span>
            </p>
            <?
                $clientScript = Yii::app()->getClientScript();
                $clientScript->scriptMap=array(
                    "jquery.js"=>false,
                    "jquery.min.js"=>false,
                    "jquery-ui.js"=>false,
                    "jquery-ui.min.js"=>false,
                ); 
            ?>
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'record-form',
                'action'=>'/master/addRecord',
                'enableClientValidation' => true,
                'htmlOptions'=>array('class'=>'record-form'),
                'clientOptions' => array(
                    'validateOnType' => false,
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {

                        if ( hasError ) 
                            return false;
                        form=$('#record-form');
                        var action = form.attr('action');

                        $.ajax({
                            url: action,
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data) {
                                console.log(data);
                                if (data.error)
                                {
                                    $('.alert-view .view').empty().append(data.error);
                                    $('.alert-view').addClass('active')
                                    
                                } else {
                                    
                                    $('.alert-view .view').empty().append(data.message);
                                    $('.alert-view').addClass('active')

                                    setTimeout(function(){
                                        $('.alert-view').removeClass('active')
                                    },5000)
                                    $.fancybox.close();
                                }
                                return false;
                            },
                            error:function(data){
                            }
                        });
                        return false;
                    }"
                ),
            )); ?>
                <?
                    echo $form->hiddenField($record,'id_master_service',array('id'=>"id_ms"));
                    echo $form->hiddenField($record,'date',array('value'=>date('Y-d-m')));
                ?>
                <div class="row">
                    <label class="req" for="name">Имя</label>
                    <?=$form->textField($record,'fio')?>
                    <?=$form->error($record,'fio')?>
                </div>
                <div class="row">
                    <label class="req" for="email">Почта</label>
                    <?=$form->textField($record,'email')?>
                    <?=$form->error($record,'email')?>
                </div>
                <div class="row">
                    <label class="req" for="phone">Телефон</label>
                    <?=$form->textField($record,'phone',array('id'=>'phone'))?>
                    <?=$form->error($record,'phone')?>
                </div>
                <div class="row">
                    <label class="req" for="comment">Комментарий</label>
                    <?=$form->textarea($record,'comment',array('style'=>'height:50px;line-height:16px;'))?>
                    <?=$form->error($record,'comment')?>
                </div>
                <input type="submit" value="Записаться" class="btn-azure">
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>