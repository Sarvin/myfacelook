<div class="modal auth-modal" style="display:none; height:300px;width:400px;" id="auth">
    <div class="steps">
        <ul>
            <li class="active" >
                Авторизация
            </li>
            <li style="display:none;">
                Восстановление пароля
            </li>
        </ul>
        <a href="#" class="close"></a>
    </div>
    <div class="main">
        <div class="step-1 active" style="margin-top:0px!important;">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'auth-form',
                'action'=>'/master/auth',
                'enableClientValidation' => true,
                'htmlOptions'=>array('class'=>'auth-form'),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( hasError ) return;
                        form = $('#auth-form');
                        var action = form.attr('action');
                        console.log(action);
                        $.ajax({
                            url: action,
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data) {
                                    if (data.success)
                                    {
                                        window.location.href='/';
                                    }
                                    if (data.error)
                                        $('#auth-form .errorMessage:first').text(data.error.password).show();
                                    console.log(data);
                                return false;
                            },
                            error:function(data){
                            }
                        });
                        return false;
                    }"
                ),
            )); ?>
                <div class="row">
                    <div class="errorMessage" style="text-align:center">

                    </div>
                </div>
                <div class="row">
                    <label class="req" for="email">E-mail</label>
                    <?=$form->textField($model,'email')?>
                    <?=$form->error($model,'email');?>
                </div>
                <div class="row">
                    <label class="req" for="password">Пароль</label>
                    <?=$form->passwordField($model,'password')?>
                    <?=$form->error($model,'password');?>
                </div>
                <div class="items">
                    <a href="/master/registration" class="item">Регистрация</a>
                    <input type="submit" value="Авторизоваться" class="btn-azure item">
                    <a href="/master/recovery" class="item recovery" style="font-size:10px;width:100px;">Забыли пароль?</a>
                </div>
                <div class="social-auth">
                <?php
                      $this->widget('application.components.UloginWidget', array(
                            'params'=>array(
                                'redirect'=>'http://'.$_SERVER['HTTP_HOST'].'/index.php?r=ulogin/login' //Адрес, на который ulogin будет редиректить браузер клиента. Он должен соответствовать контроллеру ulogin и действию login
                            )
                        )); 
                ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="step-2">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'recovery-form',
                'action'=>'/master/recovery',
                'enableClientValidation' => true,
                'htmlOptions'=>array('class'=>'auth-form'),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( hasError ) return;
                        form = $('#recovery-form');
                        var action = form.attr('action');
                        $.ajax({
                            url: action,
                            type: 'GET',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data) {
                                if ( data.error )
                                {
                                    $('.errors',form).empty().html(data.error);
                                    return;
                                }
                                if (data.success!=false)
                                {
                                    $('.form',form).fadeOut(300);
                                    $('.message',form).fadeIn(300,function(){
                                        setTimeout(function(){
                                            $.fancybox.close();
                                            $('.auth-modal .active').removeClass('active');
                                            $('.auth-modal .steps li:first').show().addClass('active')
                                            $('.auth-modal .steps li:last').hide();
                                            $('.auth-modal .step-1').addClass('active')
                                            $('.form',form).show();
                                            $('.errors',form).hide();
                                        },4000)
                                    });
                                    
                                }
                                return false;
                            },
                            error:function(data){
                            }
                        });
                        return false;
                    }"
                ),
            )); ?>
                <div class="form" style="position:relative">
                    <div class="errors" style="color: red;
                      font-size: 11px;
                      margin-left: 73px;
                      position: absolute;
                      top: -30px;">

                    </div>
                    <div class="row">
                        <label class="req" for="email">E-mail</label>
                        <?=$form->textField($recovery,'email')?>
                        <?=$form->error($recovery,'email');?>
                    </div>
                    <div class="items">
                        <input type="submit" value="Отправить" class="btn-azure item">
                    </div>
                </div>
                <div class="message" style="display:none;">
                    <p>
                        На указанный Вами электронный адрес отправленно письмо с дальнейшими указаниями по восстановлению пароля
                    </p>
                </div>
            <?php $this->endWidget(); ?>
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('.auth-modal .recovery').on('click',function(){
            $('.auth-modal .active').removeClass('active')
            $('.auth-modal .step-2,.auth-modal li:last').removeAttr('style').addClass('active');
            $('.auth-modal li:first').hide();
            return false;
        });
    })
</script>