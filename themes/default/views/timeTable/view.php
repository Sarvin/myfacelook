<?php
$this->breadcrumbs=array(
	'Time Tables'=>array('index'),
	$model->id,
);

<h1>View TimeTable #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_master',
		'id_week_day',
		'time_from',
		'time_to',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
