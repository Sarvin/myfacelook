<div class="modal auth-modal" style="display:none; height:300px;width:400px;" id="auth">
    <div class="steps">
        <ul>
            <li class="active">
                Авторизация
            </li>
        </ul>
        <a href="#" class="close"></a>
    </div>
    <div class="main">
        <div class="step-2 active">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'auth-form',
                'action'=>'/master/auth',
                'enableClientValidation' => true,
                'htmlOptions'=>array('class'=>'auth-form'),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( hasError ) return;
                        form = $('#auth-form');
                        var action = form.attr('action');
                        console.log(action);
                        $.ajax({
                            url: action,
                            type: 'POST',
                            dataType: 'json',
                            data: form.serialize(),
                            success: function(data) {
                                if ( data.error )
                                	return;
                                    if (data.success)
                                    {
                                        
                                    }
                                return false;
                            },
                            error:function(data){
                            }
                        });
                        return false;
                    }"
                ),
            )); ?>

                
                <div class="row">
                    <label class="req" for="email">E-mail</label>
                    <?=$form->textField($model,'email')?>
                    <?=$form->error($model,'email');?>
                </div>
                <div>
                    <input type="submit" value="отправить">
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>