<?php
$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	$model->id,
);

<h1>View Review #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_master',
		'id_client',
		'fio',
		'email',
		'content',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
