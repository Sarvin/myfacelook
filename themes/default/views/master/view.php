<?
    Yii::app()->clientScript->registerScriptFile('//vk.com/js/api/openapi.js?116',CClientScript::POS_END);
?>
<div class="content">
    <div class="master-view">
        <img src="<?=$model->imgBehaviorBackground->getImageUrl('main')?>" >
        <div class="info">
            <img src="<?=$model->imgBehaviorPreview->getImageUrl('view')?>" alt="" >
            <ul class="<?=$model->isOnline ? 'online' : 'offline'?>">
                <li>
                    <span class="name"><?=$model->fio?></span> <span class="city"><?=$model->city->name?></span>
                </li>
                <li>
                    <span href="#" class="place"><?=$model->prof->name.($model->work_place ? ' | '.$model->work_place : '')?></span>
                </li>
                <li>
                    <span href="#" class="comment" data-articul="<?=$photo->id?>">0</span>
                    <a href="#review" class="fancy btn-azure">Оставить отзыв</a>
                    <span class="like"><?=$totalLikes?></span>
                </li>
            </ul>
            <div class="actions">
                <a href="#" class="btn-azure" data-master="<?=$model->id?>" data-record="<?=$model->id?>">Записаться</a>
                <!-- Put this script tag to the <head> of your page -->
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

                <script type="text/javascript">
                  VK.init({apiId: 4907698, onlyWidgets: true});
                </script>

                <!-- Put this div tag to the place, where the Like block will be -->
                <div id="vk_like"></div>
                <script type="text/javascript">
                    VK.Widgets.Like("vk_like", {type: "button"},'<?=Yii::app()->request->requestUri?>');
                </script>
            </div>
        </div>
    </div>

    <div class="items">
        <div class="portfolio item">
            <p class="caption-ha"><span>Фото (<?=$photoCount?>)</span></p>
            <div class="items">
                <?
                if ($photoList){
                    if (count($photoList)%3!=0)
                        $photoList[]=new GalleryPhoto;
                    foreach ($photoList as $key => $photo) {
                        $this->renderPartial('_portfolio',array('data'=>$photo,'model'=>$model));
                        $last=$photo->id;
                    }
                } else {
                    ?>
                    <div class="empty">
                        <p>Нет загруженых фотографий</p>
                    </div>
                    <?
                }
                ?>
            </div>
            <?if ($photoCount>(int)Config::getValue('master.photo_count')){?>
                <a href="#" data-model="GalleryPhoto" data-master="<?=$model->id?>" data-last="<?=$last?>" class="more">Показать еще</a>   
            <?}?>
        
            <div class="master-record">
                <p class="caption-ha"><span>Услуги</span></p>
                <?
                    if ($serviceList){
                ?>
                <div class="row items">
                    <?
                        foreach ($serviceList as $key => $data) {
                            ?><p><?=$key?></p><?
                            $this->renderPartial('_record',array('data'=>$data,'parentName'=>$key));
                        }

                    ?>
                </div>
                <?} else {?>
                    <div class="empty">
                        <p>
                            Раздел находится в наполнении
                        </p>
                    </div>
                <?}?>
            </div>
        
        </div>
        <div class="work-place item">
            <p class="caption-ha"><span>Место работы</span></p>
            <div class="info">
                <p>
                    <?
                        foreach ($model->workTypeList as $key => $data) {
                            echo Master::workTypeList($data)." ";
                        }
                    ?>
                    <span class="name"><?=$model->work_place ? "Салон ".$model->work_place : ""?></span>
                    <span class="address"><?=$model->address?></span>
                </p>
                <!-- <img src="/media/map.png" alt="" > -->
                <div id="master_<?=$model->id?>" style="height:296px;" data-map data-adres="<?=$model->address?>">

                </div>
            </div>
            <div class="item">

                <div class="time-table">
                    <p class="caption-ha"><span>Режим работы</span></p>
                    <?if ($model->timeTable){?>
                        <ul>
                            <?
                            
                                foreach ($model->timeTable as $key => $data) {
                                    if ($data->time_from && $data->time_to)
                                        $this->renderPartial('_time_table',array('data'=>$data));
                                }

                            ?>
                        </ul>
                        <?} else {?>
                        <div class="empty">
                            <p>Не составлено</p>
                        </div>
                        <?}?>
                </div>
                <div class="exp">
                    <p class="caption-ha"><span>Образование и опыт работы</span></p>
                    <div>
                       <?=$model->wswg_body?>
                    </div>
                </div>
            </div>
        </div>
        <div class="item master-record"></div>
        <div class="item master-record"></div>
        <div class="item">
            <p class="caption-ha">
                <span>Отзывы о мастере</span>
            </p>
            <div class="reviews">
                <div class="otziv">
                    <a href="#review" class="fancy btn-azure">Оставить отзыв</a>
                </div>
                <?if ($reviewList){?>
               <?
                    foreach ($reviewList as $key => $data) {
                        $this->renderpartial('_review',array('data'=>$data));
                        $last=$data->id;
                    }
               ?>
                <?}?>           
            </div>
             <?if ($reviewCount>count($reviewList)){?>
                <a href="#" data-model="Review" data-last="<?=$last?>" data-master="<?=$model->id?>" class="more">
                    Показать еще
                </a>
            <?}?>
        </div>
    
        <div class="item another-links">
            <p class="caption-ha">
                <span>
                    Другие ссылки
                </span>
            </p>
            <div class="items">
                <?if ($model->links['Сайт']){?>
                    <a href="<?=$model->links['Сайт']?>" class="go-site item">Перейти на сайт</a>
                <?}?>
                <div class="social item">
                    <?
                        $links=$model->links;
                        unset($links['Сайт']);
                        foreach ($links as $key => $value) {
                            if ($value){
                                ?>
                                    <a href="<?=$value?>" target="_blank" class="<?=$key?>"><span></span></a>
                                <?
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="review" style="display:none;">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'review-form',
            'action'=>Yii::app()->request->requestUri,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnType' => false,
                'validateOnSubmit' => true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( hasError ) return;
                    var action = form.attr('action');

                    $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'json',
                        data: form.serialize(),
                        success: function(data) {
                            if (data.error) {
                                $('.alert-view .view').empty().append('Спасибо за Ваш отзыв!');
                                $('.alert-view').addClass('active');
                                setTimeout(function(){
                                    $('.alert-view').removeClass('active')
                                },4000);
                                $.fancybox.close();
                            }else {
                                $.each(data.errors,function(key,val){
                                    $('#Review_'+key).val(val).css('color','red');
                                    $('#Review_'+key).one('click',function(){
                                        $(this).val('').removeAttr('style');
                                    })
                                });
                            }
                            return false;
                        },
                        error:function(data){
                        }
                    });
                    return false;
                }"
            ),
        )); ?>
        <div class="main">
            <div class="review">
                <p class="caption">
                    Оставьте свой отзыв о мастере <?=$model->fio?>
                </p>
                <img src="<?=$model->imgBehaviorPreview->getImageUrl('view')?>" alt="">

                <div class="data">
                    <?=$form->hiddenField($review,'id_master')?>
                    <div class="item">
                        <?=$form->error($model,'content')?>
                        <?=$form->textArea($review,'content',array('placeholder'=>'Ваш комментарий...','row'=>0,'col'=>0))?>
                    </div>
                    <div class="items">
                        <div class="item">
                            <?=$form->error($model,'fio')?>
                            <?=$form->textField($review,'fio',array('placeholder'=>'ФИО'))?>
                        </div>
                        <div class="item">
                            <?=$form->error($model,'email')?>
                            <?=$form->textField($review,'email',array('placeholder'=>'E-mail'))?>
                        </div>
                    </div>
                </div>
                <input type="submit" value="Отправить" class="btn-azure">
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>