<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_profession')); ?>:</b>
	<?php echo CHtml::encode($data->id_profession); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('img_preview')); ?>:</b>
	<?php echo CHtml::encode($data->img_preview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_place')); ?>:</b>
	<?php echo CHtml::encode($data->work_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wswg_body')); ?>:</b>
	<?php echo CHtml::encode($data->wswg_body); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('links')); ?>:</b>
	<?php echo CHtml::encode($data->links); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?>:</b>
	<?php echo CHtml::encode($data->balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
	<?php echo CHtml::encode($data->login); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hash')); ?>:</b>
	<?php echo CHtml::encode($data->hash); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_type')); ?>:</b>
	<?php echo CHtml::encode($data->work_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cvartal')); ?>:</b>
	<?php echo CHtml::encode($data->id_cvartal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('img_background')); ?>:</b>
	<?php echo CHtml::encode($data->img_background); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	*/ ?>

</div>