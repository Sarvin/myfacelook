<div class="content">
    <div class="page-content">
        <h1 class="caption" style="font-size:24px;">
        	<?=$page->title?>
        </h1>
        <div>
            <?=$page->wswg_body?>
        </div>
        <div style="text-align:center;">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'recovery',
                'action'=>'/master/recovery?hash='.$hash,
                'enableClientValidation' => true,
                'htmlOptions'=>array('class'=>'auth-form','style'=>'display:inline-block;margin-top:30px;'),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnType' => true,
                ),
                'htmlOptions'=>array('style'=>'display:inline-block')
            )); ?>
                    <div class="row">
                        <?=$form->labelEx($recovery,'password')?>
                        <?=$form->passwordField($recovery,'password',array('style'=>'width:300px;'))?>
                        <?=$form->error($recovery,'password');?>
                    </div>
                    <div class="row">
                        <?=$form->labelEx($recovery,'verifyPassword')?>
                        <?=$form->passwordField($recovery,'verifyPassword',array('style'=>'width:300px;'))?>
                        <?=$form->error($recovery,'verifyPassword');?>
                    </div>
                    <div class="row" style="margin-top:20px;">
                        <label></label>
                        <input type="submit" value="Отправить" class="btn-azure item" style="width:323px;">
                    </div>
                </div>
            <?php $this->endWidget(); ?>
    	</div>
    </div>
</div>