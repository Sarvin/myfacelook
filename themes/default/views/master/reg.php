<div class="content">
	<div class="fix_width">
		<h1 class="caption" style="padding-bottom: 50px;text-align: center;padding-top: 10px;">
		   Регистрация мастера красоты
		</h1>
		<div class="items">
			<div class="item" style="width:300px;">
				<?=$page ? $page->wswg_body : ''?>
			</div>
	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'registration-form',
				'action'=>'/master/registration',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => true,
	                'validateOnSubmit' => true,
	            ),
	            'htmlOptions'=>array(
	            	'class'=>'item reg-form',
	            	'style'=>'margin-right:110px;'
	            ),
			)); ?>
			<div class="fields">
				<div class="row">
					<?=$form->labelEx($model,'fio')?>
					<span></span>
					<?=$form->textField($model,'fio')?>
					<?=$form->error($model,'fio')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'phone')?>
					<span></span>
					<?=$form->textField($model,'phone')?>
					<?=$form->error($model,'phone')?>
				</div>
				<div class="row gender">
					<?=$form->labelEx($model,'id_profession')?>
					<?=$form->dropDownList($model,'id_profession',CHtml::listData(Profession::model()->findAll(),'id','name'))?>
					<?=$form->error($model,'id_profession')?>
				</div>
				<div class="row gender">
					<?=$form->labelEx($model,'gender')?>
					<?=$form->dropDownList($model,'gender',Master::genderList())?>
					<?=$form->error($model,'gender')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'email')?>
					<?=$form->textField($model,'email')?>
					<?=$form->error($model,'email')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'password')?>
					<?=$form->passwordField($model,'password')?>
					<?=$form->error($model,'password')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'verifyPassword')?>
					<?=$form->passwordField($model,'verifyPassword')?>
					<?=$form->error($model,'verifyPassword')?>
				</div>
				<div class="row center">
					<label></label>
					<input style="width:424px;" type="submit" class="green-btn center" value="Зарегистрироваться">
				</div>
			</div>
		<?php $this->endWidget(); ?>
	</div>
</div>