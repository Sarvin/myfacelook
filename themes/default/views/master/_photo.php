<div class="modal photo-info" id="photo_<?=$data->id?>" data-id="<?=$data->id?>" style="display:none; height:755px;">
    <input type="hidden" id="bug_<?=$data->id?>">
    <?if ($data->id){?>    
    <div class="steps">
        <a href="#" class="close"></a>
    </div>
    <div class="master">
        <div class="items">
            <img class="item" width="72px" height="72px" src="<?=$model->getImageUrl('icon')?>" alt="">
            <div class="item">
                <span class="name">
                    <?=$model->fio?>
                </span>
                <span class="post"><?=$model->prof->name?></span>
            </div>
        </div>
    </div>
    <div class="btn-like">
        <div id="vk_like_<?=$data->id?>"></div>
    </div>
    <div class="wrap" style="position:relative">
        <a href="#" class="like" style="color:#333;position: absolute;left: 0;right: 0;border: 0;margin: auto;bottom: 30px;width: 37px;z-index: 2;text-align: center;" data-articul="<?=$data->id?>"><?=(int)$data->like?></a>
        <div class="carusel">
            <a href="#" class="prev"></a>
            <a href="#" class="next"></a>
            <div class="item">
                <img src="<?=$data->getUrl('medium')?>">
                <p><?=$data->name?></p>
            </div>
        </div>
    </div>
    <div id="vk_comments_<?=$data->id?>" style="margin:0 auto;margin-top:20px;"></div>
    <script type="text/javascript">
    $(function(){
        VK.Api.call('widgets.getComments', 
        {
            widget_api_id: "4907698", 
            url: "<?=Yii::app()->getBaseUrl(true).Yii::app()->request->requestUri?>",page_id:"photo_<?=$data->id?>"
        },function(obj){
            $('[href="#photo_<?=$data->id?>"].comment').text(obj.response.count);
        });
        $('body').one('blur','[href="#photo_<?=$data->id?>"]',function(){

        })
    })
    </script>
    <?}?>
</div>