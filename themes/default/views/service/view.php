<?php
$this->breadcrumbs=array(
	'Services'=>array('index'),
	$model->name,
);

<h1>View Service #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'parent',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
