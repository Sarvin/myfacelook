
<div class="master" data-address="г.<?=$this->city->name.' '.$data->address?>">
    <div class="info">
        <a href="<?=$data->getUrl()?>">
            <img src="<?=$data->img_preview ? $data->getImageUrl('main') : '/media/empty.png' ?>" alt="">
        </a>
        <ul>
            <li>
                <span class="name"><?=$data->fio?></span> <?=$data->area ? $data->area->city->name : ''?>
            </li>
            <li>
                <a href="#" class="place"><?=$data->prof->name?><?=$data->work_place ? ' | '.$data->work_place : ''?></a>
            </li>
            <li>
                <span>Выезжает к клиенту</span>
                <a href="#" class="comment">0</a>
            </li>
        </ul>
        <div class="clr"></div>
    </div>
    <div class="clr"></div>
    <div class="service-list">
        <ul>
            <?
                $limit=Config::getValue('catalog.master_count');
                foreach ($data->getServiceRecordList($limit) as $key => $item) {
                    foreach ($item as $key => $elem) {
                        $elem->timeView;
                        ?>
                            <li class="items">
                                <span><?=$elem->service->name?></span>
                                <span><?=$elem->timeView?></span>
                                <span><?=$elem->price?> руб.</span>
                                <a href="#" data-service="<?=$elem->id?>" class="btn-azure">Записаться</a>
                            </li>
                        <?
                    }
                    
                }
            ?>
             
        </ul>
    </div>
    <div class="clr"></div>
    <div class="baloon" style="display:none;">
        <div class="baloon-data">
            <a href="<?=$data->getUrl()?>">
                <img src="<?=$data->getImageUrl('small')?>" alt="<?=$data->fio?>">
            </a>
            <div style="display:inline-block;width:130px;">
                <a href="<?=$data->getUrl()?>">
                    <span class="baloon-fio"><?=$data->fio?></span>
                </a>
                <span class="baloon-address"><?=$data->address?></span>
                <span class="baloon-prof"><?=$data->prof->name?></span>
            </div>
        </div>
    </div>
</div>