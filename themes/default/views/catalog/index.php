<div class="content">
	<p class="caption-desc"><?=$model->name?></p>
	<div class="catalog desc">
		<?=$model->wswg_body?>
	</div>
	<div class="filter bg-def">
		<?
			$this->renderPartial('//site/_filter',array('model'=>$filter));
		?>
	</div>
</div>
<div class="content">
	<div class="items">
		<?if ($models){?>
    	<div class="master-list item">
        	<div class="infinity">
        	<?
				foreach ($models as $key => $data) {
					$this->renderPartial('_item',array('data'=>$data));
					$last=$data->id;
				}
			?>
			</div>
			<?if ($modelCount>Config::getValue('catalog.model_count')) {?>
		        <a href="#" id="step" data-id="<?=$last?>" class="more">
		            Показать еще
		        </a>
		     <?}?>
	    	</div>
	    
		    <div class="master-map item" id="map" data-city="<?=$this->city->name?>" style="width:370px;height:300px;">
		        
		    </div>
		</div>
		<?} else {?>
	    	<div class="empty">
	    		<p>Нет результатов.</p>
	    	</div>
	    <?}?>
</div>