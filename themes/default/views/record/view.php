<?php
$this->breadcrumbs=array(
	'Records'=>array('index'),
	$model->id,
);

<h1>View Record #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_master_service',
		'fio',
		'phone',
		'email',
		'time',
		'comment',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
