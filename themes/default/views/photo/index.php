<?Yii::app()->clientScript->registerScriptFile('//vk.com/js/api/openapi.js?116',CClientScript::POS_END);?>
<div class="photo-view content">
   <p class="caption">
       <?=$page->title?>
   </p>
   <div>
    <?=$page->wswg_body?>
   </div>
   <div class="items">
   	<?
   	
		foreach ($items as $key => $data) {
			$master=$data->master;
			$last=$master->id;
			$this->renderPartial('_item',array('data'=>$data,'model'=>$master));
		}
	?>
   </div>
   <a href="#" id="step" data-id="<?=$last?>" class="more">
       Показать еще
   </a>
</div>