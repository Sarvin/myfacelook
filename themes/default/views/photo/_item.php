<div class="item" style="background:<?=$data->id ? '' : 'none'?>">
    <?if ($data->id){?>
    <a href="/photo/<?=$data->id?>" class="img">
        <img src="<?=$data->getUrl('photo')?>" width="300" height="200" alt="">
    </a>
    <div class="items">
        <a href="<?=$data->gallery->master->getUrl()?>" class="item-f">
            <span class="name"><?=$data->name?></span>
            <span class="desc"><?=$data->description?></span>
        </a>
        <div class="item-f">
            <a href="#" class="like" data-articul="<?=$data->id?>"><?=(int)$data->like?></a>
            <a href="#photo_<?=$data->id?>" rel="1" class="comment fancy">0</a>
        </div>
    </div>
    <?=$this->renderPartial('//master/_photo',array('data'=>$data,'model'=>$model),true);?>
    <?}?>
</div>
