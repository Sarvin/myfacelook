<?
	$assetsUrl=$this->getAssetsUrl();
	Yii::app()->clientScript->registerScriptFile($assetsUrl.'/js/social-likes.min.js');
	Yii::app()->clientScript->registerCssFile($assetsUrl.'/css/social-likes_flat.css');
?>
<div class="view-photo">
	<div class="content" style="z-index:1">
		<div class="items">
			<div class="item img">
				<img src="<?=$model->getUrl('medium')?>">
			</div>
			<div class="item">
				<div class="master row">
					<a href="<?=$model->gallery->master->getUrl()?>">
						<img width="50" height="50" src="<?=$model->gallery->master->getImageUrl('small')?>">
					</a>
					<div class="item">
						<a href="<?=$model->gallery->master->getUrl()?>" class="name"><?=$model->gallery->master->fio?></a><br>
						<?
							foreach ($model->gallery->master->profs as $key => $data) {
								?>
									<a class="prof" href="/catalog/<?=$data->alias?>"><?=$data->name?></a>
								<?
							}
						?>
					</div>
				</div>
				<?if ($model->name){?>
				<div class="desc row">
					<span class="name"><?=$model->name?></span>
					<p class="desc">
						<?=$model->description?>
					</p>
				</div>
				<?}?>
				<?if ($model->services){?>
					<div class="service row">

						<span>Услуги:</span>
						<?
							foreach ($model->servicesRel as $key => $service) {
								?>
									<a href="<?=$service->getUrl()?>"><?=$service->name?></a>
								<?
							}
						?>
					</div>
				<?}?>
				
				<div class="social-likes row">
					<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
					<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
					<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
					<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
					<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
					<div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
				</div>
			</div>
		</div>
		<div id="vk_comments">
			<?Yii::app()->clientScript->registerScriptFile('//vk.com/js/api/openapi.js?116',CClientScript::POS_END);?>
			<script type="text/javascript">
				$(function(){
					VK.init({apiId: 4907698, onlyWidgets: true});
		            VK.Widgets.Comments("vk_comments", {limit: 1,width:600, attach: false},"photo_<?=$model->id?>");    
		            VK.Observer.subscribe('widgets.comments.new_comment', 
		                    function(num,last_comment,date,sign)
		                    {
		                        $.ajax({
		                            url:'/master/noticeComment',
		                            dataType:'json',
		                            data:{id:'<?=$model->id?>',comment:last_comment},
		                            type:'post',

		                        });
		                    }
		            );
				})
				
            </script>
		</div>
	</div>
</div>