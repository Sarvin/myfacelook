<?php

class ModelIdentifyerBehavior extends CActiveRecordBehavior
{
    private $_id_entity=false;
    
    public $attrViewName='name';
    public $enableSeo=false;

    public function getTitleView(){
        return $this->owner->{$this->attrViewName};
    }
    
    public function addRelation($owner){
        $owner->metaData->addRelation('ident', array(CActiveRecord::HAS_ONE, 'ModelIdent', 'id_model','condition'=>'id_entity=:entity','params'=>array(':entity'=>$this->id_entity)));
        return $owner;
    }

    public function beforeInit($event){
        parent::beforeInit($event);die('111');
    }

    public function afterFind($event){
        parent::afterFind($event);
        $this->addRelation($this->owner);
    }

    public function getSeo(){
        $has=$this->owner->metaData->hasRelation('ident');
        if ($has)
            if ($this->owner->ident->seo)
                return $this->owner->ident->seo;
        return new Seo;
    }

    public function getId_entity(){
        if (!$this->_id_entity)
        {
            $model=get_class($this->owner);
            if ($model=='Entity')
                $this->_id_entity=0;
            else    
                $this->_id_entity=Entity::model()->find('alias=:alias',array(':alias'=>$model))->id;
        }
        return $this->_id_entity;
    }

    public function afterSave($event) {
        parent::afterSave($event);

        $owner = $this->owner;
        if (!$owner->metaData->hasRelation('ident'))
            $this->addRelation($owner);
        if(!$owner->ident)
        {
            $data=new ModelIdent;
            $data->id_entity=$this->id_entity;
            $data->id_model=$owner->id;
            $data->save();

        } else 
            $data=$owner->ident;
        
        if ($this->enableSeo){
            
            if (isset($data->id_seo)) {
                $seo = Seo::model()->findByPk($data->id_seo);
            }

            if ( $seo === null ) {
                $seo = new Seo;
            }

            if (isset($_POST['Seo'])) {
                $seo->attributes = $_POST['Seo'];
                if($seo->save()) {
                    $data->id_seo = $seo->id;
                    $data->save();
                }
            }
        }
    }

    public function beforeDelete($event)
    {
        parent::beforeDelete($event);
        $owner = $this->getOwner();
        if ($owner->metaData->hasRelation('ident')) {
            $ident = $owner->ident;
            
            if ($ident->seo)
                $ident->seo->delete();

            if ( $ident )
                $ident->delete();

        }
        
    }
    
}