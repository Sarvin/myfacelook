<?php

class NoticeBehavior extends CActiveRecordBehavior
{

	public $contentField;
	public $theme;
	// notice types
	public $sms=false; //bool
	public $email=false; //bool

	public $emailField;
	public $smsField;

	public $smsContent;
	public $emailContent;

	public function sendMail(){

		SiteHelper::sendMail($this->theme,$this->owner->{$this->contentField},$this->owner->{$this->emailField},$from='http://myfacelook.ru');
		
	}

	public function sendSms(){

	}

	public function afterSave($event){
		parent::afterSave($event);
		if ($this->email)
			$this->sendMail();

		if ($this->sms)
			$this->sendSms();
	}

}