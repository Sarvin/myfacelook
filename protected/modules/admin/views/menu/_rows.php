<?
	Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/jquery.nestable.js',CClientScript::POS_END);
	Yii::import('ext.ESelect2.Select2');
?>

<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8')); ?>

<div class="control-group dropDown">
	<p class="title caption">
		<strong>Элементы меню</strong>
	</p>
	<?
	?>
	<?=Select2::activeMultiSelect($model, "items", ModelIdent::getModels(), array(
	        'placeholder' => 'Выберите типы работ мастера','style'=>'width:573px;'
	    ));
	?>
</div>

<?
	Yii::app()->clientScript->registerScript('sortable_select','
		$(document).on("change",".dropDown",function(){
			$(".dropDown").find("ul.select2-choices").sortable({
			    containment: "parent",
			    update: function() {
			    	var data="";
			    	$.each($(".dropDown .select2-search-choice"),function(key,val){
			    		val=$(val);
			    		data+=val.data("select2Data").id+" ";
			    	});
					$("#Menu_sortItems").val(data);
			    }
			});
		})
		
		',CClientScript::POS_END);
?>
<p class="title caption">
	<strong>
		Внешняя ссылка
	</strong>
</p>
<table class="table table-striped add">
	<tr>
		<th>
			Название
		</th>
		<th>
			Ссылка
		</th>
		<th>
		</th>
	</tr>
	<tr>
		<td><?=CHtml::textField('Menu[items][xx][name]','',array('id'=>'name'))?></td>
		<td><?=CHtml::textField('Menu[items][xx][href]','',array('id'=>'url'))?></td>
		<td><?=CHtml::button('Добавить',array('class'=>'btn','id'=>'add'))?></td>
	</tr>
</table>

<p class="title caption"><strong>Структура меню</strong></p>
<div class="dd" id="nestable3">
	<ol class="dd-list">
<?
	
	function renderChild($items){
		$content='';
		foreach ($items as $key => $data) {
			?>
			<li class="dd-item dd3-item" data-id="<?=$data->id?>" data-model="<?=$data->id_model_ident?>">
	            <div class="dd-handle dd3-handle">Drag</div>
	            <div class="dd3-content">
	            	<?=CHtml::hiddenField('Menu[outerItems]['.$data->id.'][id]',$data->id);?>
	            	<?=CHtml::textField('Menu[outerItems]['.$data->id.'][name]',$data->name);?>
	            	<?=CHtml::textField('Menu[outerItems]['.$data->id.'][href]',$data->ident ? $data->getUrl() : $data->href);?>
	            	<?=CHtml::hiddenField('Menu[outerItems]['.$data->id.'][id_model_ident]',$data->id_model_ident);?>
	            	<?=CHtml::hiddenField('Menu[outerItems]['.$data->id.'][parent]',$data->parent,array('class'=>'parent'));?>
	            	<?=CHtml::button('Удалить',array('class'=>'btn btn-error'));?>
	            </div>
	            <?
	            if ($data->childs){
			    	echo '<ol class="dd-list">';
			    		renderChild($data->childs);
			    	echo '</ol>';
		    	}?>
		    </li>	
		    <?
		    
		}
	}

	renderChild($model->parents);
?>
	</ol>
</div>
<script type="text/javascript">
	$(function(){
		
		function parent(data){
			if (data.children!=undefined){
				$.each(data.children,function(key,val){
					// var parent_id=$('[data-model="'+data.id+'"]').data('id');
					// console.log(parent_id,)
					$('[data-id="'+val.id+'"] .parent').attr('value',data.id).prop('value',data.id);
					
					if (data.children!=undefined){
						parent(val);
					}

				})
			}
		}

		$('#nestable3')
			.nestable({maxDepth:2})
			.on('change',function(e){
				var $this=$(this);
				var serialize=$this.nestable('serialize');
				$('.parent').removeAttr('value').prop('value','');
				$.each(serialize,function(key,val){
					parent(val);	
				})
			});
		$('.dropDown').on('click','.select2-search-choice-close',function(){
			return false;
		})

		$('#Menu_items').on('change',function(e){
		})

		$('#add').on('click',function(){
			var time=new Date().valueOf();
			$('#nestable3 .dd-list:first').append(
			'<li class="dd-item dd3-item" data-id="'+time+'">'+
	            '<div class="dd-handle dd3-handle">Drag</div>'+
	            '<div class="dd3-content">'+
	            	'<input type="hidden" value="'+time+'" name="Menu[outerItems]['+time+'][id]" id="Menu_items_'+time+'_id">'+
	            	'<input type="text" value="'+$('#name').val()+'" name="Menu[outerItems]['+time+'][name]" id="Menu_items_'+time+'_name">'+
	            	'<input type="text" value="'+$('#url').val()+'" name="Menu[outerItems]['+time+'][href]" id="Menu_items_'+time+'_url">'+
	            	'<input type="hidden" name="Menu[outerItems]['+time+'][parent]" id="Menu_items_'+time+'_parent" class="parent">'+
	            	'<input class="btn btn-error" name="yt5" type="button" value="Удалить">'+
	            	
	            '</div>'+
		    '</li>');
		    $('#name,#url').val('');
		})
		$('#Menu_items').on('change',function(e){
			
			if (e.added!=undefined){
				var time=(new Date().valueOf()+"")
					time=time.substr(time.length-5,5);
				var html=$(
				'<li class="dd-item dd3-item" data-id="'+time+'" data-model="'+e.added.id+'">'+
		            '<div class="dd-handle dd3-handle">Drag</div>'+
		            '<div class="dd3-content">'+
		            	'<input type="hidden" value="'+time+'" name="Menu[outerItems]['+time+'][id]" id="Menu_items_'+time+'_id">'+
		            	'<input type="text" value="'+e.added.text+'" name="Menu[outerItems]['+time+'][name]" id="Menu_items_'+time+'_name">'+
		            	'<input type="text" name="Menu[outerItems]['+time+'][href]" id="Menu_items_'+time+'_url">'+
		            	'<input type="hidden" value="'+e.added.id+'" name="Menu[outerItems]['+time+'][id_model_ident]" id="Menu_items_'+time+'_id_model_ident">'+
		            	'<input class="btn btn-error" name="yt5" type="button" value="Удалить">'+
		            	'<input type="hidden" name="Menu[outerItems]['+time+'][parent]" id="Menu_items_'+time+'_parent" class="parent">'+
		            '</div>'+
			    '</li>');

				$('#nestable3 .dd-list:first').append(html);
				
			} else {
				$('[data-model="'+e.removed.id+'"]').remove();
			}
		});

		$('#nestable3').on('click','.btn-error',function(){
			$(this).closest('[data-id]').fadeOut(300,function(){
				var val=$('option[value="'+$(this).data('model')+'"]');
				console.log(val.length,$(this).data('model'));
				if (val.length)
				{
					val.prop('selected',false).removeAttr('selected');
					$('#Menu_items').select2();
				}
				$(this).remove();

			})
		})
	});
</script>
<style type="text/css">
	.table.add{
		width: auto;
		margin-left: 30px;
	}
	.small { color: #666; font-size: 0.875em; }
	.large { font-size: 1.25em; }

	/**
	 * Nestable
	 */
	.dd-list input{
		margin: 0;
	}
	.dd { position: relative; display: block; margin: 0; padding: 0; list-style: none; font-size: 13px; line-height: 20px; }

	.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
	.dd-list .dd-list { padding-left: 30px; }
	.dd-collapsed .dd-list { display: none; }

	.dd-item,
	.dd-empty,
	.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

	.dd-handle { 
		  position: absolute;
		  margin: 0;
		  left: 0;
		  top: 0;
		  cursor: pointer;
		  width: 30px;
		  text-indent: 100%;
		  height: 96%;
		  white-space: nowrap;
		  overflow: hidden;
		  border: 1px solid #aaa;
		  background: #ddd;
		  background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
		  background: -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
		  background: linear-gradient(top, #ddd 0%, #bbb 100%);
		  border-top-right-radius: 0;
		  border-bottom-right-radius: 0;
	}
	.dd-handle:hover { color: #2ea8e5; background: #fff; }

	.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
	.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
	.dd-item > button[data-action="collapse"]:before { content: '-'; }

	.dd-placeholder,
	.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
	.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
	    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
	                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
	    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
	                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
	    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
	                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
	    background-size: 60px 60px;
	    background-position: 0 0, 30px 30px;
	}

	.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
	.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
	.dd-dragel .dd-handle {
	    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
	            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
	}

	/**
	 * Nestable Extras
	 */

	.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

	#nestable-menu { padding: 0; margin: 20px 0; }

	#nestable-output,
	#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

	#nestable2 .dd-handle {
	    color: #fff;
	    border: 1px solid #999;
	    background: #bbb;
	    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
	    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
	    background:         linear-gradient(top, #bbb 0%, #999 100%);
	}
	#nestable2 .dd-handle:hover { background: #bbb; }
	#nestable2 .dd-item > button:before { color: #fff; }

	.dd-hover > .dd-handle { background: #2ea8e5 !important; }

	/**
	 * Nestable Draggable Handles
	 */

	.dd3-content { display: block; margin: 5px 0; height: 42px; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
	    background: #fafafa;
	    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
	    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
	    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
	    -webkit-border-radius: 3px;
	            border-radius: 3px;
	    box-sizing: border-box; -moz-box-sizing: border-box;
	}
	.dd3-content:hover { color: #2ea8e5; background: #fff; }

	.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

	.dd3-item > button { margin-left: 30px; }

	.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
	    border: 1px solid #aaa;
	    background: #ddd;
	    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
	    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
	    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
	    border-top-right-radius: 0;
	    border-bottom-right-radius: 0;
	    height: 40px;
	}
	.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 0px; bottom:0; height: 18px; bottom: 0; margin: auto; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
	.dd3-handle:hover { background: #ddd; }
			
</style>