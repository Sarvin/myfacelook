
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'master-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); 
$cs=Yii::app()->clientScript;
$assets=$this->getAssetsUrl();
$cs->registerCssFile($assets.'/css/fullcalendar.css');
$cs->registerCssFile($assets.'/css/jquery.contextMenu.css');
$cs->registerCssFile($assets.'/css/bootstrap-timepicker.css');

$cs->registerScriptFile($assets.'/js/bootstrap-timepicker.js',CClientScript::POS_END);
$cs->registerScriptFile($assets.'/js/lib/moment.min.js',CClientScript::POS_END);
$cs->registerScriptFile($assets.'/js/fullcalendar.js',CClientScript::POS_END);
$cs->registerScriptFile($assets.'/js/lang-all.js',CClientScript::POS_END);
$cs->registerScriptFile($assets.'/js/jquery.contextMenu.js',CClientScript::POS_END);
$cs->registerScriptFile($assets.'/js/jquery.ui.position.js',CClientScript::POS_END);



?>
	<?php echo $form->errorSummary($model); ?>

	<?php $tabs = array(); ?>

	<?php $tabs[] = array('label' => 'Основные данные', 'content' => $this->renderPartial('_rows', array('form'=>$form, 'model' => $model), true), 'active' => false); ?>

	<?php $tabs[] = array('label' => 'Социальные сети', 'content' => $this->renderPartial('_social', array('form'=>$form, 'model' => $model), true), 'active' => false); ?>

	<?php $tabs[] = array('label' => 'Услуги мастера', 'content' => $this->renderPartial('_service', array('form'=>$form, 'model' => $model), true), 'active' => false); ?>

	<?php $tabs[] = array('label' => 'Отзывы', 'content' => $this->renderPartial('_review', array('form'=>$form, 'model' => $model), true), 'active' => false); ?>
	
	<?if (!$model->isNewRecord){?>
		<?php $tabs[] = array('label' => 'Портфолио', 'content' => $this->renderPartial('_portfolio', array('form'=>$form, 'attr'=>'id_gallery', 'model' => $model), true), 'active' => false); ?>

		<?php $tabs[] = array('label' => 'Расписание', 'content' => $this->renderPartial('_time_table', array('form'=>$form, 'model' => $model), true), 'active' => false); ?>

		<?php $tabs[] = array('label' => 'Записи', 'content' => $this->renderPartial('_records', array('form'=>$form, 'model' => $model), true), 'active' => true); ?>
	<?}?>
	<?
	?>
	<?php $tabs[] = array('label' => 'SEO', 'content' => $this->renderPartial('/seo/_form', array('form'=>$form, 'model' => $model->seo), true), 'active' => false); ?>

	<?php $this->widget('bootstrap.widgets.TbTabs', array( 'tabs' => $tabs)); ?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton('Сохранить', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo TbHtml::linkButton('Отмена', array('url'=>'/admin/master/list')); ?>
	</div>

<?php $this->endWidget(); ?>

