<?php
$this->menu=array(
	array('label'=>'Добавить','url'=>array('create')),
);
?>

<h1>Управление <?php echo $model->translition(); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'master-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>TbHtml::GRID_TYPE_HOVER,
    'afterAjaxUpdate'=>"function() {sortGrid('master')}",
    'rowHtmlOptionsExpression'=>'array(
        "id"=>"items[]_".$data->id,
        "class"=>"status_".(isset($data->status) ? $data->status : ""),
    )',
	'columns'=>array(
		array(
			'header'=>'Фото',
			'type'=>'raw',
			'value'=>'CHtml::link(TbHtml::imageCircle($data->imgBehaviorPreview->getImageUrl("icon")),"/admin/master/update/id/".$data->id)'
		),
		array(
			'name'=>'fio',
			'type'=>'raw',
			'value'=>'CHtml::link($data->fio,"/admin/master/update/id/".$data->id)'
		),
		array(
			'name'=>'id_area',
			'type'=>'raw',
			'value'=>'$data->area->name',
			'filter'=>CHtml::listData(Area::model()->findAll(),'id','name'),
		),
		array(
			'name'=>'gender',
			'type'=>'raw',
			'value'=>'Filter::getGenderList($data->gender)',
			'filter'=>Filter::getGenderList()
		),
		'work_place',
		'phone',
		array(
			'name'=>'work_type',
			'type'=>'raw',
			'value'=>'Master::workTypeList($data->gender)',
			'filter'=>Master::workTypeList()
		),
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'Master::getStatusAliases($data->status)',
			'filter'=>Master::getStatusAliases()
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}'
		),
	),
)); ?>

<?php if($model->hasAttribute('sort')) Yii::app()->clientScript->registerScript('sortGrid', 'sortGrid("master");', CClientScript::POS_END) ;?>