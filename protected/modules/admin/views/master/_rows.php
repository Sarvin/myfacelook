	<?
		Yii::import('ext.ESelect2.Select2');
		if ($model->isNewRecord){
	?>
		<div class="alert alert-success ">
			Раздел портфолио, записи и расписание станут доступны после сохранения
		</div>
	<?}?>

	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'img_preview'); ?>
		<?php echo $form->fileField($model,'img_preview', array('class'=>'span3')); ?>
		<div class='img_preview'>
			<?php if ( !empty($model->img_preview) ) echo TbHtml::imageRounded( $model->imgBehaviorPreview->getImageUrl('small') ) ; ?>
			<span class='deletePhoto btn btn-danger btn-mini' data-modelname='Master' data-attributename='Preview' <?php if(empty($model->img_preview)) echo "style='display:none;'"; ?>><i class='icon-remove icon-white'></i></span>
		</div>
		<?php echo $form->error($model, 'img_preview'); ?>
	</div>

	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'img_background'); ?>
		<?php echo $form->fileField($model,'img_background', array('class'=>'span3')); ?>
		<div class='img_preview'>
			<?php if ( !empty($model->img_background) ) echo TbHtml::imageRounded( $model->imgBehaviorBackground->getImageUrl('small') ) ; ?>
			<span class='deletePhoto btn btn-danger btn-mini' data-modelname='Master' data-attributename='Background' <?php if(empty($model->img_background)) echo "style='display:none;'"; ?>><i class='icon-remove icon-white'></i></span>
		</div>
		<?php echo $form->error($model, 'img_background'); ?>
	</div>
	
	<?php echo $form->textFieldControlGroup($model,'fio',array('class'=>'span8 name')); ?>

	<?php echo $form->textFieldControlGroup($model,'alias',array('class'=>'span8 alias')); ?>

	<?php echo $form->textFieldControlGroup($model,'phone',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'email',array('class'=>'span8','maxlength'=>255)); ?>
	
	<?php echo $form->dropDownListControlGroup($model,'id_area',CHtml::listData(Area::model()->findAll(),'id','name'),array('class'=>'span8')); ?>

	<?php echo $form->textFieldControlGroup($model,'address',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'work_place',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'balance',array('class'=>'span8')); ?>
	<div class="control-group">
		<label>Специализации мастера</label>
		<?=Select2::activeMultiSelect($model, "professions",CHtml::listData(Profession::model()->findAll(),'id','name'), array(
	       'placeholder' => 'Выберите специализации мастера','style'=>'width:573px;'
	   	));?>
	</div>

	<?php echo $form->dropDownListControlGroup($model,'id_profession',CHtml::listData(Profession::model()->findAll(),'id','name'),array('class'=>'span8')); ?>

	<?php echo $form->dropDownListControlGroup($model,'gender',Filter::getGenderList(),array('class'=>'span8')); ?>
	<div class="control-group">
		<label>Занятости мастера</label>
		<?=Select2::activeMultiSelect($model, "workTypeList",Master::workTypeList(), array(
		        'placeholder' => 'Выберите типы работ мастера','style'=>'width:573px;'
		    ));
		?>
	</div>
	<?php echo $form->dropDownListControlGroup($model, 'status', Master::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>

	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'wswg_body'); ?>
		<?php $this->widget('appext.ckeditor.CKEditorWidget', array('model' => $model, 'attribute' => 'wswg_body', 'config' => array('width' => '100%')
		)); ?>
		<?php echo $form->error($model, 'wswg_body'); ?>
	</div>

	<script type="text/javascript">
	$(function(){
		setTimeout(function(){
			$('.cke_contents').height(300)	
		},1000)
		
	})
	</script>