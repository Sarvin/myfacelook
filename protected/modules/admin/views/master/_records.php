
<div id="calendar">

</div>
<?
	$record=new Record;
	$record->time=$data->time_from;
?>

<div id="myModal" class="modal fade hide">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Новая запись</h4>
      </div>
      <div class="modal-body" style="max-height:490px">
      	<div class="alert alert-danger fade">

      	</div>
        <div class="control-group">
			<label>Услуга</label>
			<?
				echo CHtml::openTag('select',array('name'=>'Record[id_master_service]','class'=>'span12'));
				echo $model->getServiceList();
				echo CHtml::closeTag('select');
			?>
		</div>

		<?=TbHtml::activeTextFieldControlGroup($record,'fio',array('class'=>'span12'))?>
		<?=TbHtml::activeTextFieldControlGroup($record,'phone',array('class'=>'span12'))?>
		<?=TbHtml::activeEmailFieldControlGroup($record,'email',array('class'=>'span12'))?>
		<?//=TbHtml::activeTextFieldControlGroup($record,'date',array('class'=>'span12'))?>
		<?=TbHtml::activeTextFieldControlGroup($record,'time',array('class'=>'span12'))?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="cancel" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="save">Сохранить изменения</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

		var dayNames=<?=$model->getWeekDayShorNames()?>;
		$('#Record_time').timepicker({
			minuteStep: 5,
	        template: 'modal',
	        appendWidgetTo: 'body',
	        showSeconds: false,
	        showMeridian: false,
		});

		$('#Record_date').datepicker();

		$('#myModal #cancel').on('click',function(){
			$('#myModal .alert').empty().removeClass('in');
			$('#myModal input,#myModal select').val('')
		});

		$('#myModal #save').on('click',function(){
			var $this=$('#myModal');
			$.ajax({
				url:'/admin/master/addRecord?id='+($this.data('id')!=undefined ? $this.data('id') : ''),
				type:'POST',
				dataType:'json',
				data:{
					Record:{
						fio:$('#Record_fio').val(),
						date:$this.data('date')+" "+$('#Record_time').val(),
						id_master_service:$('[name="Record[id_master_service]"]').val(),
						phone:$('#Record_phone').val(),
						email:$('#Record_email').val(),
					}
				},
				success:function(data){
					if (!data.errors)
					{
						data.start=moment(data.start,'YYYYMMDD');
						data.end=moment(data.end,'YYYYMMDD');

						$('#calendar').fullCalendar('refetchEvents');
						$('#myModal .alert').empty().removeClass('in');
						$('#myModal').modal('hide');
						$('#myModal').data('date',null);
						$('#myModal').data('id',null);
						$('#myModal input,#myModal select').val('')
					} else {
						$('#myModal .alert').empty();
						$('#myModal .alert').append(data.errors).addClass('in');
					}
				}
			})
		});

		var contextMenuActions={
			request:function(url,data,callback){
				var $this=$(this);
				$.ajax({
					url:url,
					data:data,
					dataType:'json',
					success:function(data){
						if (data.errors)
							alert(data.errors);
						else {
							if(callback)
								callback.apply($this,[data]);
						}
					}
				})
			},
			openDialog:function(id,date){
				
				var title=id!=undefined ? 'Редактирование записи' : 'Новая запись';
				$('#myModal .modal-title').text(title);
				$('#myModal').data('id',id);
				$('#myModal').data('date',date);
				$('#myModal').modal('show');
			},
			add:function(date){
				var base=contextMenuActions;
				$('#myModal input,#myModal select').val('')
				$('#myModal').data('id','');
				$('#myModal').data('date','');
				base.openDialog.apply(this,[undefined,date]);
			},
			edit:function(id,date){
				var base=contextMenuActions;
				$.ajax({
					url:'/admin/master/getEventData?id='+(id!=undefined ? id : ''),
					dataType:'json',
					success:function(data){
						if (!data.errors)
						{
							$('#Record_fio').val(data.success.fio);
							$('#Record_time').val(data.success.start);
							$('[name="Record[id_master_service]"]').val(data.success.id_master_service);
							$('#Record_phone').val(data.success.phone);
							$('#Record_email').val(data.success.email);
							$('#Record_date').val(data.success.date);
							$('#Record_date').datepicker();
							$('#Record_time').timepicker('setTime', data.success.start);
							base.openDialog.apply(this,[id,date]);
						}
					}
				})
			},
			remove:function(id){
				var base=contextMenuActions;
				if (confirm('Вы действительно хотите удалить запись на сеанс?'))
					base.request('/admin/master/removeRecord?id='+id,false,function(){$('#calendar').fullCalendar('refetchEvents');});
			},
			status:function(id,status){
				console.log(id,status);
				$.ajax({
					url:'/admin/master/changeStatus?id='+(id!=undefined ? id : '')+"&status="+status,
					dataType:'json',
					success:function(data){
						if (!data.errors)
						{
							console.log(data);
						}
					}
				})
			},
			checkDate:function(date){
				var time=date;
				time=time.split("-");
				var thisDate=time[0]+"-"+time[1]+"-"+time[2];
				return new Date(thisDate).getTime()>=new Date().getTime()-1000*60*60*24;
			}

		}
		$('#calendar').fullCalendar({
			dayNamesShort:dayNames,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			lang: 'ru',
			buttonIcons: false, // show the prev/next text
			weekNumbers: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			unselectAuto:true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: '/admin/master/getEvents?id=<?=$model->id?>',
			eventAfterRender:function(calEvent,element){
				element.data('event-data',calEvent.id);
			},
			eventMouseover:function(event, jsEvent, view){

			},
			eventDrop: function(event, delta, revertFunc) {

				if (contextMenuActions['edit'].apply(this,[event.id,]));

		        if (!confirm("Are you sure about this change?")) {
		            revertFunc();
		        }

		    },
			eventAfterAllRender:function(){
				$.contextMenu({
			        selector: '#calendar .fc-content', 
			        callback: function(key, options) {
			            var id=$(this).closest('a').data('eventData');
						var date=$(this).closest('a').data('fcSeg').event.start.format('YYYY-MM-DD');
			            var split=key.split('-');
			            var check=contextMenuActions['checkDate'].apply(this,[date]);
			            
			            if (check && contextMenuActions[split[0]]!=undefined)
			            	contextMenuActions[split[0]].apply(this,[id,split[1] ? split[1] :date]);
			            
			            if (!check)
			            	alert('Редактировать или добавлять даты задним числом запрещено!');

			        },
			        items: {
			            "edit": {"name": "Редактировать", "icon": "edit"},
			            "step1": "---------",
			            "remove": {"name": "Удалить", "icon": "cut"},
			            "step2": "---------",
			            "status": {
			                "name": "Статус", 
			                "items": {
			                    "status-1": {
		                    		"name": "Новая",
			                	},
			                    "status-2": {"name": "Ожидание"},
			                    "status-3": {"name": "Выполнено"},
			                    "status-4": {"name": "Отменена"},
			                }
			            },
			        }
			    });
				
				$.contextMenu({
			        selector: '#calendar .fc-day', 
			        callback: function(key, options) {
			            var index=$(this).closest('td').index();
						var date=$(this).closest('table').find('tr:first td').eq(index).data('date');

						if (contextMenuActions['checkDate'].apply(this,[date]) && contextMenuActions[key]!=undefined)
			            	contextMenuActions[key].apply(this,[date]);
			            else 
			            	alert('Редактировать или добавлять даты задним числом запрещено!');

			        },
			        items: {
			            "add": {"name": "Добавить", "icon": "add"},
			        }
			    });


			}
		});

	});


</script>