<div class="control-group reviews">
	<div class="container-fluid">
		<div class="control-group">
			<button type="button" class="btn btn-success add-review">Добавить отзыв</button>
		</div>
	</div>
	<div class="container-fluid items">
		<div class="bordered"></div>
	<?
		foreach ($model->reviews as $key => $data) {
		?>
			<div class="control-group bordered">
				<div class="control-group">
					<?=TbHtml::textField('Master[reviewData]['.$data->id.'][fio]',$data->fio,array('class'=>'span6','placeholder'=>'ФИО'));?>
					<button style="margin-bottom:5px;float:right" class="btn btn-danger del-review" type="button">Удалить</button>
				</div>
				<?=TbHtml::textFieldControlGroup('Master[reviewData]['.$data->id.'][email]',$data->email,array('class'=>'span6','placeholder'=>'Почта'))?>
				<?=TbHtml::dropDownListControlGroup('Master[reviewData]['.$data->id.'][status]',$data->status,Review::getStatusAliases(),array('class'=>'span6','placeholder'=>'Почта'))?>
				<?=TbHtml::textareaControlGroup('Master[reviewData]['.$data->id.'][content]',$data->content,array('class'=>'span12','cols'=>6,'rows'=>7,'placeholder'=>'Тест отзыва...'))?>
			</div>
		<?}
	?>
	</div>
	<div class="control-group bordered hide">
		<div class="control-group">
			<?=TbHtml::textField('Master[reviewData][xx][fio]','',array('class'=>'span6','placeholder'=>'ФИО'));?>
			<button style="margin-bottom:5px;float:right" class="btn btn-danger del-review" type="button">Удалить</button>
		</div>
		<?=TbHtml::textFieldControlGroup('Master[reviewData][xx][email]','',array('class'=>'span6','placeholder'=>'Почта'))?>
		<?=TbHtml::textareaControlGroup('Master[reviewData][xx][content]','',array('class'=>'span12','cols'=>6,'rows'=>7,'placeholder'=>'Тест отзыва...'))?>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$(document).on('click','.del-review',function(){
			$(this).closest('.bordered').fadeOut(300,function(){
				$(this).remove();
			});
		})
		
		$(document).on('click','.add-review',function(){
			$('.reviews .hide')
				.clone(true)
				.insertBefore('.reviews .items .bordered:first')
				.removeClass('hide');

			var elems=$('.reviews .items .control-group').eq(0).find('input,textarea');
			$.each(elems,function(key,val){
				val=$(val);
				var name=val.prop('name');
				val.attr('name',name.replace('xx','xx_'+new Date().getTime()));
			});
		});

	})
</script>