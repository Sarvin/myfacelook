<div class="control-group">
	<p>
		<span class="name">
			<?=$data->fio?>
		</span>
		<span class="email">
			<a href="mailto:<?=$data->email?>"><?=$data->email?></a>
		</span>
	</p>
	<p class="desc">
		<?=$data->content?>
	</p>
</div>