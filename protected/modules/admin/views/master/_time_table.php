
<table>
	<thead>
		<th>День недели</th>
		<th>Начало рабочего дня</th>
		<th>Конец рабочего дня</th>
	</thead>
	<tbody>
	<?
		foreach ($model->timeTable as $key => $data) {
			$this->renderPartial('_time_table_item',array('data'=>$data, 'day'=>$key));
		}

	?>
	</tbody>
</table>
<script type="text/javascript">
	$(function(){
		$('.timeTable_from,.timeTable_to').timepicker({
			minuteStep: 5,
	        template: 'modal',
	        appendWidgetTo: 'body',
	        showSeconds: false,
	        showMeridian: false,
		});
	})
</script>