<div class="control-group services">
	<table>
		<tdead>
			<tr>
				<th>
					Услуга
				</th>
				<th>
					Длительность
				</th>
				<th>
					Описание
				</th>
				<th>
					Цена
				</th>
				<th>
				</th>
			</tr>
		</thead>
		<tbody>
				<?
					$data=!$model->errors ? $model->masterService : $model->services;
					$data[]=new MasterService;

					$serviceArray=Service::getList();
					foreach ($data as $key => $model) {
						$this->renderPartial('_serviceItem',array('data'=>$model,'serviceArray'=>$serviceArray,'class'=>'none','id'=>$key));
					}
					$this->renderPartial('_serviceItem',array('data'=>$model,'serviceArray'=>$serviceArray,'class'=>'hide','id'=>'xx'));
				?>

			
		</tbody>
	</table>
	<?=TbHtml::button('Добавить',array('id'=>'add_row'))?>
</div>

<style type="text/css">
	table button{
		margin-bottom: 10px!important;
	}
</style>
<script type="text/javascript">
	
	$(function(){

		var opt=$('select option:selected');

		$('table').on('click','button',function(){
			$(this).closest('tr').fadeOut(400,function(){
				$(this).remove()
			})
		});

		$('.session').timepicker({
			minuteStep: 5,
	        template: 'dropdown',
	        showSeconds: false,
	        showMeridian: false,
		});
		$('body').on('change','.session',function(){
			$(this).attr('value',$(this).text());	
		})
		$('#add_row').on('click',function(){

			$('.services tbody .hide').closest('tbody')
				.append('<tr class="" data-num="xx">'+
							'<td>'+
								'<select class="span12" name="Master[services][xx_2][id_service]" id="Master_services_xx_id_service">'+
									'<option value="">Выберите услугу</option>'+
									'<optgroup label="Стрижки">'+
									'<option value="2">Детская стрижка</option>'+
									'<option value="3">Горячая стрижка</option>'+
									'<option value="4">Мужская стрижка модельная</option>'+
									'<option value="5">Мужские стрижки машинкой</option>'+
									'</optgroup>'+
								'</select>	'+
							'</td>'+
							'<td>'+

								'<input class="session" type="text" name="Master[services][xx_2][time]" id="Master_services_xx_time">'+
							'</td>'+
							'<td>'+
								'<input class="span12" type="text" name="Master[services][xx_2][desc]" id="Master_services_xx_desc">'+
							'</td>'+
							'<td>'+
								'<input class="span12" type="text" name="Master[services][xx_2][price]" id="Master_services_xx_price">'+
							'</td>'+
							'<td>'+
								'<button class="span12 btn" name="yt2" type="button">Удалить</button>'+
							'</td>'+
						'</tr>')
				$('.session:last').timepicker({
					minuteStep: 5,
			        template: 'dropdown',
			        showSeconds: false,
			        showMeridian: false,
				});

			var elems=$('.services tbody tr').not('.hide').find('input,select');
			$.each(elems,function(key,val){
				val=$(val);
				var name=val.prop('name');
				var id=val.prop('id');
				val.attr('name',name.replace('xx','xx_'+val.closest('tr').index()))
					.attr('id',id.replace('xx','xx_'+val.closest('tr').index()))
			});
		})

	})

</script>