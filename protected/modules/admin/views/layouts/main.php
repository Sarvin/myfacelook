<!DOCTYPE html>
<html lang="en">
	<head>
	  <meta charset="utf-8">
	  <title><?php echo CHtml::encode(Yii::app()->config->get('app.name')).' | Admin';?></title>
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>

        <?php
            $menuItems = array(
                array('label'=>'Меню', 'url'=>array('/admin/menu')),
                array('label'=>'Мастера', 'url'=>array('/admin/master')),
                array('label'=>'Услуги', 'url'=>array('/admin/service')),
                array('label'=>'Локации', 'items'=>array(
                        array('label'=>'Города','url'=>array('/admin/city/list')),
                        array('label'=>'Районы','url'=>array('/admin/area/list'))
                    )
                ),
                array('label'=>'Профессии', 'url'=>array('/admin/profession')),
                array('label'=>'Отзывы', 'url'=>array('/admin/review')),
                array('label'=>'Страницы', 'url'=>array('/admin/page')),

                array('label'=>'Настройки', 'url'=>array('/admin/config')),
            );
        ?>
        <?php
            $userlogin = Yii::app()->user->name ? Yii::app()->user->name : Yii::app()->user->email;
            $this->widget('bootstrap.widgets.TbNavbar', array(
                'color'=>'inverse', // null or 'inverse'
                'brandLabel'=> CHtml::encode(Yii::app()->name),
                'brandUrl'=>'/',
                'collapse'=>true, // requires bootstrap-responsive.css
                'items'=>array(
                    array(
                        'class'=>'bootstrap.widgets.TbNav',
                        'items'=>$menuItems,
                    ),
                    array(
                        'class'=>'bootstrap.widgets.TbNav',
                        'htmlOptions'=>array('class'=>'pull-right'),
                        'items'=>array(
                            array('label'=>'Выйти ('.$userlogin.')', 'url'=>'/user/logout'),
                        ),
                    ),
                ),
            ));
        ?>

        <?php echo $content;?>

	</body>
</html>
