<?php
$this->breadcrumbs=array(
    "Структура сайта"=>array('/admin/page/list'),
    'Создание',
);

	$this->menu=array(
	    array('label'=>'Список','url'=>array('/admin/page/list')),
	);
?>

<h2>Новая страница</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>