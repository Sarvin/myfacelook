<?php echo $form->textFieldControlGroup($model,'title',array('class'=>'span12 name')); ?>

<?
	if ($model->status!=4){
	 	echo $form->textFieldControlGroup($model,'alias',array('class'=>'span12 alias')); 
	 	echo $form->dropDownListControlGroup($model, 'status', Page::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); 
	}
?>

<div class='control-group'>
	<?php echo CHtml::activeLabelEx($model, 'wswg_body'); ?>
	<?php $this->widget('appext.ckeditor.CKEditorWidget', array(
		'model' => $model,
		'attribute' => 'wswg_body',
		'config' => array(
			'width' => '99%'
		),
	)); ?>
	<?php echo $form->error($model, 'wswg_body'); ?>
</div>

