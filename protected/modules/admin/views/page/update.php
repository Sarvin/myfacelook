<?php
$this->breadcrumbs=array(
    "Страницы"=>array('/admin/page/list'),
    'Редактирование',
);

$this->menu=array(
    array('label'=>'Список страниц','url'=>array('/admin/page/list',)),
    array('label'=>'Добавить новую','url'=>array('create')),
);

?>

<h2><?php echo $model->title; ?></h2>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>