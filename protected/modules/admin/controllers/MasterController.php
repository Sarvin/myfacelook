<?php

class MasterController extends AdminController
{
	public function event(){
		echo rand(10,100);
	}

	public function actionAddRecord($id=null){
		
		$model=$id ? Record::model()->findByPk($id) : new Record;

		$model->attributes=$_POST['Record'];
		if ($model->validate()) {
			$model->save();
		}
		if ($model->id)
		{
			$time=explode(':',$model->time);
            $end=date('Y-m-d H:i',strtotime($model->date)+3600*$time[0]+60*$time[1]);
			$response=array(
				'title'=>$model->masterService->name,
				'start'=>date('Y-m-d',strtotime($model->date)),
				'end'=>date('Y-m-d',$end),
				'attr'=>$model->errors,
				'id'=>$model->id,
			);	
		}
		
		//$response['errors']=CHtml::errorSummary($model);
		$response['errors']=CHtml::errorSummary($model);

		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionRemoveRecord($id){
		$response=array();
		$response['success']=Record::model()->deleteByPk($id);
		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionChangeStatus($id,$status){

		$response['success']=Yii::app()->db->createCommand()
								->update('{{record}}',array('status'=>$status),'id=:id',array(':id'=>$id));
		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionGetEvents(){
		if ($_GET['id'])
		{
			$model=Master::model()->findByPk($_GET['id']);
			if ($model)
			{
				$events=$model->getEvents();
				foreach ($events as $key => $data) {
					$events[$key]['color']=Record::getColor($events[$key]['status']);
					unset($events[$key]['status']);
				}
				echo CJSON::encode($events);
			}

			Yii::app()->end();
		}
		echo CJSON::encode(array());
		Yii::app()->end();
	}

	public function actionGetEventData($id){
		$model=Record::model()->findByPk($id);
		$response['errors']=!$model;
		if ($model)
		{
			$response['success']=array(
				'fio'=>$model->fio,
				'start'=>date('H:i',strtotime($model->date)),
				'phone'=>$model->phone,
				'email'=>$model->email,
				'id_master_service'=>$model->id_master_service
			);
		}
		
		echo CJSON::encode($response);

		Yii::app()->end();
	}

}
