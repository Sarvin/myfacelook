<?php

class PhotoController extends FrontController
{
	public $layout='//layouts/main';
	
	public function actionIndex(){

		if (Yii::app()->request->isAjaxRequest){

            $criteria=new CDbCriteria;

            $criteria->addCondition('t.id>:step');
            $criteria->params[':step']=$_GET['step'];
            $criteria->limit=Config::getValue('photo.limit');
            $criteria->order='id asc';
            $criteria->distinct=true;
            $masters=Master::model()->findAll($criteria);
            
            $criteria->addCondition('gallery_id=:gallery');
			$criteria->limit=3;
            
            $content=array();
            $last=false;

            $items=array();
			foreach ($masters as $key => $data) {
				$criteria->params[':gallery']=$data->gallery->id;
				$result=GalleryPhoto::model()->findAll($criteria);

				foreach ($result as $key => $value) {
					$items[]=$this->renderPartial('_item',array('data'=>$value,'model'=>$data),true);
				}
				$last=$data->id;
			}
			if (count($items)%3>0)
					$items[]=new GalleryPhoto;
            $response['success']=$items ? implode(' ',$items) : "";
            $response['step']=$last;
            $response['count']=count($items);
            echo CJSON::encode($response);
            Yii::app()->end();
        }
        $page=Page::model()->findByAlias('photo');
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/photo.js');
		$limit=Config::getValue('photo.limit');
		
		$criteria=new CDbCriteria;
		$criteria->order='RAND()';
		$masters=Master::model()->findAll($criteria);
		
		$criteria->order='id desc';
		$criteria->limit=3;
		$criteria->addCondition('gallery_id=:gallery');
		$criteria->limit=3;

		$items=array();
		foreach ($masters as $key => $data) {
			$criteria->params[':gallery']=$data->gallery->id;
			$result=GalleryPhoto::model()->findAll($criteria);
			foreach ($result as $key => $value) {
				$items[]=$value;
			}
		}

		if (count($items)%3>0)
			$items[]=new GalleryPhoto;
		
		$this->render('index',array('items'=>$items,'page'=>$page));
		
	}

	public function actionView($id){

		$model=GalleryPhoto::model()->findByPk($id);
		$tags=CHtml::listData($model->services,'id','name');
		$tags=implode(', ',$tags);
		$profs=implode(', ',CHtml::listData($model->gallery->master->profs,'id','name'));
		
		$seo=new Seo;
		$seo->meta_title='Фотография работы '.$profs.': '.$tags.'. Мастер '.$model->gallery->master->fio;
		$seo->meta_keys=$tags.', фото';
		$seo->meta_desc=$profs.': .'.$model->gallery->master->fio.' На фото: '.$tags.'. Свежие образы и идеи от лучших мастеров красоты.';
		
		$this->registerSeo($seo);
		$this->render('view',array('model'=>$model));
		
	}

}
