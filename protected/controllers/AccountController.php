<?php

class AccountController extends FrontController
{
    public function accessRules()
    {
        return array(
            array('deny',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('registration'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
            	'actions'=>'index',
                'users'=>array('?'),
            ),
        );
    }

    public function actionIndex(){
    	
    	$this->render('index');
    }

    public function actionRegistration(){

    	$this->render('registration');

    }

    public function actionLogout(){
    	Yii::app()->user->logout();
    }

    public function actionLogin(){
        
    }

}