<?php

class CatalogController extends FrontController
{
    public function actionIndex($parent='',$service=''){

        $filter=Filter::init();
        if (Yii::app()->request->isAjaxRequest){
            $criteria=new CDbCriteria;
            
            if ($filter->criteria)
                $criteria->mergeWith($filter->criteria);

            $criteria->addCondition('t.id>:step');
            $criteria->params[':step']=$_GET['step'];
            $criteria->limit=Config::getValue('catalog.master_count');
            $criteria->order='id asc';
            
            $masters=Master::model()->findAll($criteria);
            $content=array();
            $last=false;

            foreach ($masters as $key => $data) {
                $content[]=$this->renderPartial('_item',array('data'=>$data),true);
                $last=$data->id;
            }
            
            $response['success']=implode(' ',$content);
            $response['step']=$last;
            echo CJSON::encode($response);
            Yii::app()->end();
        }
        
        if (isset($_GET['Filter'])){
            $filter->criteria=false;
            $filter->redirect='/catalog';
            $filter->attributes=$_GET['Filter'];
            $criteria=$filter->getCriteria();
            $filter->saveInSession();
            if ($filter->redirect){
                $this->redirect($filter->redirect);
            }
        }
        
        if ($parent || $service){
            $model=Service::model()->findByAlias($service ? $service : $parent);
            $filter->term=$model->name;
            if (!$model && !$service){
                $model=Profession::model()->findByAlias($parent);
                $filter->term=$model->name;
            }
            $filter->criteria=false;
        }
        if (empty($model))
        {
            $model=Page::model()->findByAlias('catalog');
            $filter->model=$model;
        }
        
        $criteria=$filter->getCriteria();
        $filter->saveInSession();
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/catalog.js?v=1',CClientScript::POS_END);
        $this->breadcrumbs=array('каталог');
        $modelCount=Master::model()->count($criteria);
        $criteria->limit=Config::getValue('catalog.master_count');
        $models=Master::model()->findAll($criteria);
        $filter->regSeo();
    	$this->render('index',array('model'=>$model,'page'=>$page, 'models'=>$models,'filter'=>$filter,'modelCount'=>$modelCount));
    }

    public function actionAutocomplite(){

        $search=new Filter;
        $query=$_GET['query'];
        if ($query){
            $search->term=$query;

            $response['suggestions']=$search->getAutoCompliteData();
            echo CJSON::encode($response);
        }
    }

}