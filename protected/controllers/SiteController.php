<?php

class SiteController extends FrontController
{
	public $layout = '//layouts/main';

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

	public function actionTestLogin(){
		$this->render('test');
	}

	public function actionGetArea(){
		
		$city=$_POST['Filter']['id_city'];
		$models=Area::model()->findAll('id_city=:city',array(':city'=>$city));
		$res="";

		foreach ($models as $key => $data) {
			echo CHtml::tag('option', array('value'=>$data->id),CHtml::encode($data->name),true);
		}

	}

	public function actionIndex()
	{	

        $this->title = Yii::app()->config->get('app.name');
        
        $criteria=new CDbCriteria;
        $criteria->addCondition('parent=0');
        $criteria->limit=Config::getValue('service.main_page_limit');

        $services = Service::model()->published()->findAll('parent=0');
        
        $limit=Config::getValue('master.main_page_limit');
        
        $criteria=new CDbCriteria;
        $criteria->limit=$limit ? $limit : 5;
        $criteria->order='Rand()';

        $masters = Master::model()->published()->findAll($criteria);
        $filter = new Filter;
        $page=Page::model()->findByAlias('main');
        $this->model=$page;
        $menu=Menu::model()->findByPk(5);
		$this->render('index',array(
				'services'=>$services,
				'masters'=>$masters,
				'filter'=>$filter,
				'page'=>$page,
				'menu'=>$menu
			)
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}