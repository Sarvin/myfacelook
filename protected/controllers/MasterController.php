<?php

class MasterController extends FrontController
{
	public $layout='//layouts/simple';

	public function beforeAction($action){
		if (parent::beforeAction($action))
		{
			if (Yii::app()->user->isMaster && !Yii::app()->request->isAjaxRequest){
				$master=Master::model()->findByPK(Yii::app()->user->id);
				if ($master){
					$master->updateStatus();
					Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/user_status.js?v=2');
				}
			}
			return true;
		}
		return false;
	}

	public function getPhoto($id){
		$photo=GalleryPhoto::model()->findByPk($id);
		echo $this->renderPartial('_photo',array('model'=>$photo),true);
	}

	public function actionNoticeComment(){
		$id=$_POST['id'];
		$photo=GalleryPhoto::model()->findByPk($id);
		$email=$photo->gallery->master->email;
		if ($email){
			SiteHelper::sendMail('Новый комментарий',
				'Новый коментарий к работе '.Yii::app()->request->hostInfo.$photo->getUrl().'<br>'.$_POST['comment'],$email,'http://myfacelook.ru');
		}
	}

	public function actionRecovery($hash=''){
		$response['success']=false;
		if (!$hash){
			$model=new RecoveryForm;
			$model->email=$_GET['RecoveryForm']['email'];
			$valid=$model->validate();
			$response['success']=$valid;
			if ($valid)
			{
				$model->sandHash();
				$model->saveHash();
				$response['success']=true;
			} else {
				$response['error']=CHtml::errorSummary($model);
			}
			echo CJSON::encode($response);
			Yii::app()->end();
		} else {
			
			$model=Master::model()->find('hash=:hash',array(':hash'=>$hash));
			$this->regSeo=false;
			$this->model=$model;
			if ($model){
				if ($_POST['RecoveryForm'])
				{

					$recovery=new RecoveryForm;
					$recovery->attributes=$_POST['RecoveryForm'];
					$recovery->hash=$hash;
					$recovery->email=$model->email;
					$flag=$recovery->updatePassword();
					if ($flag){
						
						Yii::app()->request->cookies['auth'] = new CHttpCookie('auth', true);
						$this->redirect('/');

					} else {
						$recovery->password=null;
						$recovery->verifyPassword=null;
						$page=Page::model()->findByAlias('recovery');
						$this->render('recovery',array('hash'=>$hash,'recovery'=>$recovery,'page'=>$page));
					}
				} else {
					
					$this->breadcrumbs=array('Восстановление пароля');
					$this->regSeo=false;
					$this->model=$model;
					$page=Page::model()->findByAlias('recovery');
					$recovery=new RecoveryForm;
					$recovery->attributes=$model->attributes;
					$recovery->password='';
					$this->render('recovery',array('hash'=>$hash,'recovery'=>$recovery,'page'=>$page));

				}
			}
		}
	}

	public function actionLk(){
		$assetsUrl=$this->getAssetsUrl();
		$cs=Yii::app()->clientScript;
		$cs->registerCssFile($assetsUrl.'/master_lk/bootstrap.css');
		$cs->registerCssFile($assetsUrl.'/master_lk/lineicons/style.css');
		$cs->registerCssFile($assetsUrl.'/master_lk/style.css');
		$cs->registerCssFile($assetsUrl.'/master_lk/style-responsive.css');
		$cs->registerCssFile($assetsUrl.'/master_lk/lineicons/style.css');
		$this->render('lk/index');
	}

	public function actionView($alias)
	{
		
		$model=Master::model()->findByAlias($alias);
		$this->regSeo=false;
		$this->model=$model;
		if (!$model)

		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/owl.carousel.js');
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/owl.carousel.css');
		
		$review=new Review;

		if ($_POST['Review'] && Yii::app()->request->isAjaxRequest)
		{
			$review->attributes=$_POST['Review'];
			if ($valid=$review->validate())
				$review->save();
			$response=array('error'=>$review->id!==null,'errors'=>$review->errors);
			echo CJSON::encode($response);
			Yii::app()->end();
		}
		
		$review->id_master=$model->id;

		$criteria=new CDbCriteria;
		$criteria->addCondition('id_master=:master');
		$criteria->params[':master']=$model->id;
		
		$reviewCount=Review::model()->published()->count($criteria);
		$criteria->limit=Config::getValue('master.review_count');
		$reviewList=Review::model()->published()->findAll($criteria);

		$serviceList=$model->getServiceRecordList();
		$criteria=new CDbCriteria;
		$criteria->addCondition('gallery_id=:gallery');
		$criteria->params[':gallery']=$model->gallery->id;
		$photoCount=GalleryPhoto::model()->count($criteria);
		$criteria->limit=Config::getValue('master.photo_count');
		$criteria->order="rank";
		$photoList=GalleryPhoto::model()->findAll($criteria);

		$totalLikes=Yii::app()->db->createCommand()
			->select('SUM(`like`) as `like`')
			->from('{{gallery_photo}}')
			->where('gallery_id=:gallery',array(':gallery'=>$model->gallery->id))
			->queryRow();

		$this->breadcrumbs=array('Каталог мастеров'=>'/catalog',$model->fio);
		
		$model->regSeo();

		$this->render('view',
			array(
				'model'=>$model,
				'review'=>$review,
				'serviceList'=>$serviceList,
				'photoList'=>$photoList,
				'reviewList'=>$reviewList,
				'photoCount'=>$photoCount,
				'reviewCount'=>$reviewCount,
				'totalLikes'=>(int)$totalLikes['like'],
			)
		);
	}

	public function actionRecordForm($id,$step=1){

		if ($step>1){
			$model=MasterService::model()->findByPk($id)->master;
			$record=new Record;
			$record->id_master_service=$id;
		} else {
			$model=Master::model()->findByPk($id);
			$record=new Record;
		}

		$response['view']=$this->renderPartial('//forms/_recordForm',array('model'=>$model,'record'=>$record, 'id'=>$id,'step'=>$step),true,true);
		foreach ($model->getTimeTable() as $key => $data) {
			$response['timeTable'][$data->id_week_day]=$data->time_from && $data->time_to;
		}
		echo CJSON::encode($response);
	}

	public function actionLoadMore($model,$last,$master=''){

		if ($master)
			$master=Master::model()->findByPk($master);
		
		$oldLast=$last;

		$criteria=new CDbCriteria;

		$criteria->addCondition('t.id>:id');
		$criteria->params[':id']=$last;

		if ($model=="GalleryPhoto")
		{
			$criteria->addCondition('gallery_id=:gallery');
			$criteria->params[':gallery']=$master->gallery->id;
			$view='_portfolio';
			$limit=Config::getValue('master.photo_count');
		} 
		if ($model=='Review'){
			$view='_review';
			$limit=Config::getValue('master.review_count');
			$criteria->addCondition('id_master=:master and status=1');
			$criteria->params[':master']=$master->id;
		}

		if ($model=='Master'){
			
			$filter=new Filter;
			$filter->attributes=$_GET['Filter'];
			
			$limit=Config::getValue('catalog.model_count');

			$criteria=$filter->getCriteria();
			$criteria->addCondition('t.id>:id');
			$criteria->params[':id']=$last;

			$view='//catalog/_item';

		}

		$criteria->limit=$limit;
		$criteria->order='t.id asc';
		$models=$model::model()->findAll($criteria);
		$content=array();
		
		if ($model='GalleryPhoto' && count($models)%3>1)
			$models[]=new GalleryPhoto;

		foreach ($models as $key => $data) {
			$content[]=$this->renderPartial($view,array('data'=>$data,'model'=>$master),true);
			$last=$data->id;
		}

		$response['items']=implode(' ',$content);

		if (count($models)==$limit)
			$response['last']=$last;

		echo CJSON::encode($response);
	}

	public function actionGetTimeTable($id_ms){
		
		$model=MasterService::model()->findByPK($id_ms);
		$response['timeTable']=array();

		foreach ($model->master->getTimeTable() as $key => $data) {
			$response['timeTable'][$data->id_week_day]=$data->time_from && $data->time_to;
		}

		echo CJSON::encode($response);
	}

	public function actionUpdateStatus(){
		if (Yii::app()->user->isMaster){
			$master=Master::model()->findByPK(Yii::app()->user->id);
			$master->updateStatus();
		}
	}

	public function actionGetTimeView($date='',$id_ms){
		$date=$date=='' ? date('Y-m-d') : date('Y-m-d',strtotime($date));
		$model=MasterService::model()->findByPK($id_ms);
		echo CJSON::encode(array('timeTable'=>$model->master->getFreeRecordTime($date,$id_ms),'price'=>$model->price));
	}

	public function actionTestModal(){

		$this->render('//forms/_recordForm');
	}

	public function actionAddRecord(){

		$response=array('error'=>true);
		
		if ($_POST['Record']){
			$model=new Record;
			$model->attributes=$_POST['Record'];
			$model->date=date('Y-m-d H:i',strtotime($model->date));
			$params=explode(' ',$model->date);
			$model->time=$params[1];
			if ($model->validate())
				$model->save();
			
			$response['error']=CHtml::errorSummary($model);

			if (!$response['error'])
				$response['message']="<p>Вам придет смс оповещение на указанный Вами номер с подтверждением или отказом мастера</p>";
		}

		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionAddLike($id){
		$photo=GalleryPhoto::model()->findByPk($id);
		$response=array();
		if ($photo && !Yii::app()->request->cookies['like_'.$photo->id]->value){
			$photo->like+=1;
			$photo->save();
			$email=$photo->gallery->master->email;

			if ((string)Yii::app()->request->cookies['like_'.$photo->id]==="")
				$photo->sendLikeNotice($email);

			Yii::app()->request->cookies['like_'.$photo->id] = new CHttpCookie('like_'.$photo->id, true);
		} else {
			
			$income=!Yii::app()->request->cookies['like_'.$photo->id]->value ? 0 : -1;
			$photo->like+=$income;
			$photo->save();
			Yii::app()->request->cookies['like_'.$photo->id] = new CHttpCookie('like_'.$photo->id, false);
		}
		$response['success']=$photo->like;

		echo CJSON::encode($response);
		Yii::app()->end();

	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Master');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));

	}

	public function actionAuth(){
		$responce=array();
		if ($_POST['AuthForm']){
			$auth=new AuthForm;
			$auth->attributes=$_POST['AuthForm'];
			if($auth->authenticate())
			{
				$responce['success']=true;
			} else {
				$responce['error']=$auth->errors;
			}
		}

		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionLogout($uri){
		Yii::app()->user->logout();
		$this->redirect($uri);
	}

	public function actionRegistration(){
		$model=new RegForm;
		$page=Page::model()->findByAlias('auth');
		if ($_POST['RegForm']){
			$model->attributes=$_POST['RegForm'];
			if ($model->save())
			{
				$this->redirect('/');
			}
		}
		$this->render('reg',array('model'=>$model,'page'=>$page));
	}

}
