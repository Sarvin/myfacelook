<?php

/**
* This is the model class for table "{{area}}".
*
* The followings are the available columns in table '{{area}}':
    * @property integer $id
    * @property integer $id_city
    * @property string $name
*/
class Area extends EActiveRecord
{
    public function tableName()
    {
        return '{{area}}';
    }


    public function rules()
    {
        return array(
            array('id_city', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, id_city, name', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'city'=>array(self::BELONGS_TO,'City','id_city'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_city' => 'Город',
            'name' => 'Название района',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_city',$this->id_city);
		$criteria->compare('name',$this->name,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
