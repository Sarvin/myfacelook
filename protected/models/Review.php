<?php

/**
* This is the model class for table "{{review}}".
*
* The followings are the available columns in table '{{review}}':
    * @property integer $id
    * @property integer $id_master
    * @property integer $id_client
    * @property integer $fio
    * @property integer $email
    * @property integer $content
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Review extends EActiveRecord
{
    public function tableName()
    {
        return '{{review}}';
    }


    public function rules()
    {
        return array(
            array('id_master, id_client, status, sort', 'numerical', 'integerOnly'=>true),
            array('create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('content,fio,email','required'),
            array('id, id_master, id_client, fio, email, content, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'master'=>array(self::BELONGS_TO,'Master','id_master'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master' => 'Мастер',
            'id_client' => 'Клиент',
            'fio' => 'ФИО',
            'email' => 'Почта',
            'content' => 'Комментарий',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
            'notice'=>array(
                'class' => 'application.behaviors.NoticeBehavior',
                'email'=>true,
                'emailField'=>"email",
                'theme'=>'Новый отзыв',
                'contentField'=>'content',
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master',$this->id_master);
		$criteria->compare('id_client',$this->id_client);
		$criteria->compare('fio',$this->fio);
		$criteria->compare('email',$this->email);
		$criteria->compare('content',$this->content);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
