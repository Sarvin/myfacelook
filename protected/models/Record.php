<?php

/**
* This is the model class for table "{{record}}".
*
* The followings are the available columns in table '{{record}}':
    * @property integer $id
    * @property integer $id_master_service
    * @property string $fio
    * @property string $phone
    * @property string $email
    * @property string $time
    * @property string $comment
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Record extends EActiveRecord
{
    public $old_date;
    public $old_time;
    public $old_id_service;
    public $id_master;

    public function tableName()
    {
        return '{{record}}';
    }

    public function afterFind(){
        parent::afterFind();
        $this->old_time=$this->time;
        $this->old_date=$this->date;
        return true;
    }

    public static function getColor($cl){
        $colors=array('#6BC269','#3C84B8','#B83C3C');
        return $colors[$cl];
    }

    public function rules()
    {
        return array(
            array('id_master_service, status, sort', 'numerical', 'integerOnly'=>true),
            array('fio, phone, email, time,date', 'length', 'max'=>255),
            array('comment, create_time, update_time', 'safe'),
            array('fio, phone, email, date, time','required'),
            // The following rule is used by search().
            array('id, id_master_service, fio, phone, email, time, comment, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'masterService'=>array(self::BELONGS_TO,'MasterService','id_master_service'),
        );
    }

    public function validateTime()
    {

        $this->time=$this->masterService->time;
        $data=Yii::app()->db->createCommand()
            ->select('*')
            ->from('{{record}}')
            ->where("date(date)='".date('Y-m-d',strtotime($this->date))."' and id_master_service in (select id from {{master_service}} where id_master=".$this->masterService->id_master.") and id!=".($this->id ? $this->id : 0))
            ->order('date asc')
            ->queryAll();
        $time=explode(':',$this->time);
        
        $time[0]=date('H',strtotime($this->time));
        $time[1]=date('i',strtotime($this->time));
        $timeTable=$this->masterService->master->timeTable;
        $weekDay=$timeTable[date('w',strtotime($this->date))];
        
        if ($weekDay){
            $start=strtotime(date('H:i',strtotime($weekDay->time_from)));
            $end=strtotime(date('H:i',strtotime($weekDay->time_to)));
            $curTime=strtotime(date('H:i',strtotime($this->date)));
        } else {
            $this->addError('timeTable',"В данный день мастер не работает!");
            return false;
        }
        if (!($start<=$curTime && $end>=$curTime))
        {
            $this->addError('timeTable',"Режим работы мастера с {$weekDay->time_from} по {$weekDay->time_to} выберите время в этом промежутке!");
            return false;
        }
        
        if (!$data)//если нет сеансов/записей
            return true;

        $date_end_this=date('Y-m-d H:i',strtotime($this->date)+3600*$time[0]+60*$time[1]);

        if (count($data)>1){
            
            $prev=$data;
            $next=$data;
            $time=explode(':',$data[0]['time']);
                $date_end_prev=date('Y-m-d H:i',strtotime($data[0]['date'])+3600*$time[0]+60*$time[1]);
            if (strtotime($date_end_this)<=strtotime($data[0]['date']))
            {
                return true;
            }

            unset($prev[count($data)-1]);
            unset($next[0]);
            
            for ($i=0; $i <count($data)-1; $i++) { 
                $prev=$data[$i];
                $next=$data[$i+1];
                $time=explode(':',$prev['time']);
                $date_end_prev=date('Y-m-d H:i',strtotime($prev['date'])+3600*$time[0]+60*$time[1]);
                if (strtotime($date_end_prev)<=strtotime($this->date) 
                    && (strtotime($date_end_this)<=strtotime($next['date']))) {
                    return true;
                }
            }

            $time=explode(':',$date[count($data)-1]['time']);
            $date_end_last=date('Y-m-d H:i',strtotime($data[count($data)-1]['date'])+3600*$time[0]+60*$time[1]);
            if (strtotime($date_end_last)<=(strtotime($this->date))) {
                return true;
            }
        } else {

            $time=explode(':',$data[0]['time']);
            $date_end_record=date('Y-m-d H:i',strtotime($data[0]['date'])+3600*$time[0]+60*$time[1]);

            if (strtotime($date_end_this)<=strtotime($data[0]['date']))
            {
                return true;
            }
            if (strtotime($this->date)>=(strtotime($date_end_record))){
                return true;
            }
        }

        $this->addError('time','В выбранный вами интервал времени назначен сеанс!');
        return false;
    }

    public function beforeSave(){
        if (parent::beforeSave()){
            if ( $this->scenario=='insert' )
               return $this->validateTime();

            if ( $this->scenario=='update' && ($this->old_time!=$this->time || $this->date!=$this->old_date) )
                return $this->validateTime();
            return true;
        }
        return false;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master_service' => 'Услуга',
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'date' => 'Дата сеанса',
            'time' => 'Начало сеанса',
            'comment' => 'Коментарий',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }

    public function getContent(){
        return $content="Поступила новая заявка на запись:<br>Услуга - ".$this->masterService->service->name.'<br>Дата проведения - '.date('d-m-Y H:i',strtotime($this->date)).'<br><a href="http://myfacelook.ru">Myfacelook.ru</a>';
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
            'notice'=>array(
                'class' => 'application.behaviors.NoticeBehavior',
                'email'=>true,
                'emailField'=>"email",
                'theme'=>'Запись на сеанс',
                'contentField'=>'content',
            ),
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master_service',$this->id_master_service);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function getStatusAliases($id=false){
        $list=array(
            0=>'Ожидание',
            1=>'Отменено',
            2=>'Реализованно',
        );
        return $id ? $list[$id] : $list;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
