<?
class MasterIdentity extends CUserIdentity
{
    private $_id;
    public $record;
    public function authenticate()
    {
        
        $record=Master::model()->resetScope()->findByAttributes(array('email'=>$this->username));
        $this->record=$record;
        Yii::app()->user->model='Master';
        Yii::app()->user->titleField='fio';
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if(Yii::app()->getModule('user')->encrypting($this->password)!=$record->password)
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id=$record->id;
            $this->setState('username', $record->fio);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}
?>