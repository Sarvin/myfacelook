<?php

/**
* This is the model class for table "{{photo_tag}}".
*
* The followings are the available columns in table '{{photo_tag}}':
    * @property integer $id
    * @property integer $id_photo
    * @property integer $id_tag
*/
class PhotoTag extends EActiveRecord
{
    public function tableName()
    {
        return '{{photo_tag}}';
    }

    public function rules()
    {
        return array(
            array('id_photo, id_tag', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_photo, id_tag', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_photo' => 'Фото',
            'id_tag' => 'Тег',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_photo',$this->id_photo);
		$criteria->compare('id_tag',$this->id_tag);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
