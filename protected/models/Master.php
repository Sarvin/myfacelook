<?php

/**
* This is the model class for table "{{master}}".
*
* The followings are the available columns in table '{{master}}':
    * @property integer $id
    * @property integer $id_profession
    * @property string $img_preview
    * @property string $address
    * @property integer $gender
    * @property string $work_place
    * @property string $wswg_body
    * @property string $links
    * @property string $phone
    * @property string $email
    * @property integer $balance
    * @property string $login
    * @property string $password
    * @property string $hash
    * @property integer $work_type
    * @property integer $id_area
    * @property string $img_background
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Master extends EActiveRecord
{
    const CHECK_TIME=3;
    private $_services;
    private $_portfolio;
    private $_timeTable;
    private $_work_types=false;
    private $_professions=false;

    public $reviewData;

    public $active='main';
    
    public function getWorkTypeList(){
        
        if (!$this->_work_types)
            $this->_work_types=CHtml::listData($this->work_types,'id_work_type','id_work_type');
        return $this->_work_types;

    }

    public function setWorkTypeList($data){
        $this->_work_types=$data;
    }

    public function getPortfolio(){
        return $this->_portfolio;
    }

    public function setPortfolio($data){
        $this->_portfolio=$data;
    }

    public function getServices(){
        return $this->_services;
    }

    public function setServices($data){

        foreach ($data as $key => $item) {
            $this->_services[$key]=new MasterService;
            $this->_services[$key]->attributes=$item;
        }

        $this->_services=$this->_services ? $this->_services : array();
    }

    public function getProfessions(){
        return $this->_professions=$this->profs ? CHtml::listData($this->profs,'id','id') : array();
    }

    public function setProfessions($data){
        return $this->_professions=$data;
    }

    public function tableName()
    {
        return '{{master}}';
    }

    public function getIsOnline(){
        if (date('Y-m-d')==date('Y-m-d',strtotime($this->action_time))){

            $cur_hour=date('H');
            $cur_min=date('i');
            
            $act_hour=date('H',strtotime($this->action_time));
            $act_min=date('i',strtotime($this->action_time));
            return ($cur_min+$cur_hour)-($act_min+$act_hour)<=3;
        }
        return false;
    }

    public function rules()
    {
        return array(
            array('id_profession, gender, balance, work_type, id_area, status, sort, id_gallery', 'numerical', 'integerOnly'=>true),
            array('email','unique'),
            array('address, work_place, phone, email, alias, fio, login, password, hash', 'length', 'max'=>255),
            array('timeTable','checkTimeTable','message'=>'Расписание дожно заполнено полностью'),
            array('img_preview, wswg_body, professions, links, img_background, create_time, update_time, services, portfolio, timeTable, reviewData,workTypeList,action_time', 'safe'),
            array('fio,phone,email,gender','required'),
            // The following rule is used by search().
            array('id, id_profession, img_preview, address, gender, work_place, wswg_body, links, phone, email, balance, login, password, hash, work_type, id_area, img_background, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function sendHash(){
        $message="Перейти";
    }

    public function defaultScope(){

        if (Yii::app()->controller->module->id!="admin")
            return array(
                'condition'=>'id_city=:city',
                'join'=>'inner join {{area}} a on a.id=id_area',
                'params'=>array(':city'=>Yii::app()->controller->city->id),
            );
        else
            return array();
    }

    public function getFreeRecordTime($date,$id_ms)
    {

        $result=Yii::app()->db->createCommand()
            ->selectDistinct("t.time, date")
            ->from('{{record}} t')
            ->join('{{master_service}} ms','ms.id=t.id_master_service')
            ->where('t.`date` like(:date) and id_master=:master',array(':date'=>$date.'%',':master'=>$this->id))
            ->order('STR_TO_DATE(date,"%Y-%m-%d %H:%i") asc')
            ->queryAll();
        //var_dump($result,$date);die();
        $weekDay=date('w',strtotime($date));
        $dayShedule=$this->timeTable[$weekDay];
        
        if (!$dayShedule)
            return false;

        $result[]=array('date'=>$dayShedule->time_to);

        $service=MasterService::model()->findByPk($id_ms);
        $break=false;
        
        $serviceDuration=explode(':',$service->time);
        $duration=($serviceDuration[0]*60*60+1)+60*$serviceDuration[1];//Продолжительность проведения сеанса выбранной услуги
        $time=$dayShedule->time_from;//время записи на сеанс 8:00,8:15,8:30 и до конца рабочего дня
        
        $increment_time=60*15;// 15 минут

        $return=array();
        $counter=0;
        while(!$break){
            $sessionEnd=strtotime(date('H:i',strtotime($time)+$duration));
            if ($sessionEnd<=strtotime($result[$counter]['date'])) {
                $return[]=$time;
            } else {
                
                $hours=date('H',strtotime($result[$counter]['time']))*60*60;
                $minutes=date('i',strtotime($result[$counter]['time']))*60;
                $time=date('H:i',strtotime($result[$counter]['date'])+$hours+$minutes);
                $counter++;
            }
            
            $time=date('H:i',strtotime($time)+$increment_time);//увеличиваю время записи на $increment_time;

            $break=$sessionEnd>strtotime($dayShedule->time_to);
        }
        return $return;
    }

    public function regSeo(){
        
        if (!$this->ident->seo->meta_title)
            $this->ident->seo->meta_title="Мастер красоты и ".$this->prof->name.' - '.$this->fio.' г. Тюмень';
        
        if (!$this->ident->seo->meta_keys)
        {
            $services=CHtml::listData($this->services,'id','name');
            $professions=CHtml::listData($this->profs,'id','name');
            $this->ident->seo->meta_keys=implode(', ',$services).', '.implode(', ',$professions).', мастер Тюмени, цены, отзывы, рейтинг, подбор бесплатно, запись на приём, лучшие, Myfacelook';
        }

        if (!$this->ident->seo->meta_desc){
            $this->ident->seo->meta_desc='Позвонить мастеру '.$this->fio.' и записаться на прием онлайн. А также узнайте цены и посмотрите фотографии!';
        }
        
        Yii::app()->controller->registerSeo($this->ident->seo);
    }

    public function getTimeTable(){

        if (!$this->_timeTable){

            foreach (self::weekDays() as $day_id => $data) {

                    $day=TimeTable::model()->find('id_master=:master and id_week_day=:day order by id_week_day asc',array(':master'=>$this->id,':day'=>$day_id));
                    if (!$day)
                        $day=new TimeTable;
                    $day->id_master=$this->id;
                    $day->id_week_day=$day_id;
                    $result[]=$day;
            }
            $this->_timeTable=$result;
        }
        return $this->_timeTable;
    }

    public function getWeekDayShorNames(){
        $result=array('Пн','Вт','Ср','Чт','Пт','Сб','Вс');
        $return=array();
        $count=0;

        foreach ($this->timeTable as $key => $data) {
            if ($data->time_from && $data->time_to){
                $return[]=$result[$count].' ('.$data->time_from.' - '.$data->time_to.') ';
                $count++;
            }
            else {
                $return[]=$result[$count].' - Выходной';
                $count++;
            }
        }
        $result=array($return[6]);
        $result=array_merge($result,$return);
        return json_encode($result);
    }

    public function setTimeTable($data){
        if (is_array($data))
        {
            $this->_timeTable=null;
            foreach ($data as $key => $value) {
                $model=new TimeTable;
                $model->attributes=$value;
                $model->id_week_day=$key;
                $model->id_master=$this->id;
                $this->_timeTable[]=$model;
            }
            
        }
    }

    public function relations()
    {
        return array(
            'masterService'=>array(self::HAS_MANY,'MasterService', 'id_master','order'=>'id'),
            'reviews'=>array(self::HAS_MANY,'Review', 'id_master'),
            'profs'=>array(self::MANY_MANY,'Profession', '{{master_profession}}(id_master,id_profession)'),
            'prof'=>array(self::BELONGS_TO,'Profession', 'id_profession'),
            'area'=>array(self::BELONGS_TO,'Area','id_area'),
            'work_types'=>array(self::HAS_MANY,'MasterWork','id_master'),
            'services'=>array(self::MANY_MANY,'Service','{{master_service}}(id_master,id_service)'),
        );
    }

    public function getCity(){
        return $this->area ? $this->area->city->name : '';
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'fio'=>"ФИО мастера",
            'id_profession' => 'Основаная специализация',
            'img_preview' => 'Аватар',
            'address' => 'Адрес',
            'gender' => 'Пол',
            'work_place' => 'Место работы',
            'wswg_body' => 'Описание',
            'links' => 'Ссылки',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'workTypeList'=>'Типы занятости мастера',
            'balance' => 'Баланс',
            'timeTable'=>'Расписание',
            'services'=>'Услуги',
            'portfolio'=>'Портфолио',
            'login' => 'Логин',
            'password' => 'Пароль',
            'hash' => 'Хешь',
            'work_type' => 'Тип работы',
            'id_area' => 'Район',
            'img_background' => 'Фон мастера',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'imgBehaviorPreview' => array(
				'class' => 'application.behaviors.UploadableImageBehavior',
				'attributeName' => 'img_preview',
				'versions' => array(
					'icon' => array(
						'centeredpreview' => array(90, 90),
					),
					'small' => array(
						'resize' => array(200, 180),
					),
                    'main' => array(
                        'adaptiveResize' => array(183, 183),
                    ),
                    'view' => array(
                        'adaptiveResize' => array(125, 125),
                    ),
                    'review' => array(
                        'adaptiveResize' => array(146, 146),
                    ),
				)
			),
			'imgBehaviorBackground' => array(
				'class' => 'application.behaviors.UploadableImageBehavior',
				'attributeName' => 'img_background',
				'versions' => array(
					'icon' => array(
						'centeredpreview' => array(90, 90),
					),
					'small' => array(
						'resize' => array(200, 180),
					),
                    'main'=>array(
                        'adaptiveResize' => array(980, 450),
                    )
				),
			),
            'modelIdentifyer'=>array(
                'class'=>'application.behaviors.ModelIdentifyerBehavior',
                'attrViewName'=>'fio',
                'enableSeo'=>true,
            ),
            'galleryBehaviorPlans' => array(
                'class' => 'appext.imagesgallery.GalleryBehavior',
                'idAttribute' => 'id_gallery',
                'tags'=>true,
                'versions' => array(
                    'small' => array(
                        'adaptiveResize' => array(90, 90),
                    ),
                    'medium' => array(
                        'resize' => array(600),
                    ),
                    'view' => array(
                        'adaptiveResize' => array(183, 183),
                    ),
                    'photo'=>array(
                        'adaptiveResize' => array(500, 200),
                    )
                ),
                'name' => true,
                'description' => true,
            ),
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_profession',$this->id_profession);
		$criteria->compare('img_preview',$this->img_preview,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('work_place',$this->work_place,true);
		$criteria->compare('wswg_body',$this->wswg_body,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('work_type',$this->work_type);
		$criteria->compare('id_area',$this->id_area);
		$criteria->compare('img_background',$this->img_background,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function init(){
        parent::init();
        $this->links=self::socialLinkList();
        return true;
    }

    public function afterFind(){
        parent::afterFind();

        if (!is_array($this->links))
            $this->links=unserialize($this->links);
        return true;
    }

    public function beforeSave(){
        if (parent::beforeSave())
        {
            $this->links=serialize($this->links);
            return true;
        }
        return false;
    }

    private function saveService(){
        $del=array();
        foreach ($this->services as $key => $value) {
            if ($value['id_service'])
            {
                $model=MasterService::model()->findByPk($key);
                if (!$model)
                    $model=new MasterService;

                $model->attributes=$value;
                $model->id_master=$this->id;
                $model->save();
                $del[]=$model->id;
            }
        }
        if ($del)
            MasterService::model()->deleteAll('id not in ('.implode(',',$del).') and id_master=:master',array(':master'=>$this->id));
    }

    public function getEvents(){
        
        if ($this->masterService)
            $data=CHtml::listData($this->masterService,'id','id');

        if ($data){
            $result=Yii::app()->db->createCommand()
                ->select('t.id as id,s.name as title,date as start, concat(t.date,t.time) as end,t.status')
                ->from('{{record}} t')
                ->join('{{master_service}} ms','ms.id=t.id_master_service')
                ->join('{{service}} s','s.id=ms.id_service')
                ->where('id_master_service in ('.implode(',', $data).')')
                ->queryAll();
            return $result;
        } else {
            return array();
        }
    }

    public function checkTimeTable(){
        if (is_array($this->_timeTable))
        {
            foreach ($this->timeTable as $id_day => $data) {
                $count=strlen(trim($data->time_from))+strlen(trim($data->time_to));
                if ( ($count===strlen(trim($data->time_from)) || $count===strlen(trim($data->time_to)) ) && $count>0 && !$this->errors['timeTable']){
                    $this->addError('timeTable','Заполниет время '.($data->time_from ? 'конца' : 'начала').' рабочего времени - '.self::weekDays($id_day));
                }
                
                if ( strtotime($data->time_from) > strtotime($data->time_to) )
                    $this->addError('timeTable','Время начала рабочего дня не может быть больше времени конца рабочего дня '.self::weekDays($id_day));
            }
        }
    }

    public function saveTimeTable(){

        if (is_array($this->_timeTable)){
            TimeTable::model()->deleteAll('(id_master is null) or id_master=:master',array(':master'=>$this->id));
            foreach ($this->timeTable as $id_day => $data) {
                if ($data['time_from'] && $data['time_to'])
                {
                    $model=TimeTable::model()->find('id_master=:master and id_week_day=:day',array(':master'=>$this->id,':day'=>$id_day));
                    if (!$model)
                        $model=new TimeTable;
                    $model->attributes=$data->attributes;
                    $model->save();
                }
            }
        }
    }

    public function saveReview(){
        
        if ($this->reviewData && is_array($this->reviewData)){
            foreach ($this->reviewData as $id_review => $data) {
                $model=Review::model()->find('id_master=:master and id=:id',array(':master'=>$this->id,':id'=>$id_review));
                
                if (!$model)
                    $model=new Review;
                
                $model->attributes=$data;
                $model->id_master=$this->id;
                $model->save();
            }
        }
    }

    public function saveWorkTypes(){
        if ($this->workTypeList){
            MasterWork::model()->deleteAll('id_master=:master',array(':master'=>$this->id));
            foreach ($this->workTypeList as $key => $data) {
                $model=new MasterWork;
                $model->id_master=$this->id;
                $model->id_work_type=$data;
                $model->save();
            }
        }
    }

    public function afterSave(){
        parent::afterSave();
        if (!is_array($this->links) && $this->links!="")
            $this->links=unserialize($this->links) ? unserialize($this->links) : $this->links;
        
        $this->saveService();
        $this->saveTimeTable();
        $this->saveReview();
        $this->saveWorkTypes();

        if ($this->_professions){
            MasterProfession::model()->deleteAll('id_master=:master',array(':master'=>$this->id));
            foreach ($this->_professions as $key => $data) {
                $model=new MasterProfession;
                $model->id_master=$this->id;
                $model->id_profession=$data;
                $model->save();
                //var_dump($model->errors);die();
            }
        }

    }

    public function beforeDelete(){
        if (parent::beforeDelete())
        {
            MasterService::model()->deleteAll('id_master=:master',array(':master'=>$this->id));
            TimeTable::model()->deleteAll('id_master=:master',array(':master'=>$this->id)); 
            return true;
        }
        return false;
    }

    public function getServiceRecordList($limit=false){//Вывод  списка услуг мастера для записи пользователем на сианс через фронт

        $parents=Yii::app()->db->createCommand()
            ->selectDistinct('parent')
            ->from('{{service}} s')
            ->join('{{master_service}} ms','ms.id_service=s.id')
            ->where('id_master=:master',array(':master'=>$this->id))
            ->queryAll();
        $parents=array_map(function($n){return $n['parent'];},$parents);

        $criteria=new CDbCriteria;
        
        if ($limit)
            $criteria->limit=$limit;

        $criteria->addCondition('parent=:parent and id_master=:master');
        $criteria->select="*,t.id as id,s.name as serviceName";
        $criteria->params[':parent']=0;
        $criteria->params[':master']=$this->id;
        $criteria->join='inner join {{service}} s on t.id_service=s.id';

        foreach ($parents as $key => $data) {

            $criteria->params[':parent']=$data;
            
            $parentName=Service::model()->findByPk($data)->name;

            $services[$parentName]=MasterService::model()->findAll($criteria);
        }
        return $services ? $services : array();
    }

    public function getServiceList(){
        $result=Yii::app()->db->createCommand()
            ->select('t.id as id,s.name as name,t.time as time')
            ->from('{{master_service}} t')
            ->join('{{service}} s','s.id=t.id_service')
            ->where('id_master=:id',array(':id'=>$this->id))
            ->queryAll();
        $content='';
        foreach ($result as $key => $value) {
            $content.=CHtml::openTag('option',array('data-time'=>$value['time'],'value'=>$value['id']));
            $content.=$value['name'];
            $content.=CHtml::closeTag('option');
        }
        return $content;
    }

    public static function weekDays($id=null){
        $data=array(
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        );
        return $id!==null ? $data[$id] : $data;
    }

    public static function socialLinkList($id=null){
        $data=array(
            'Сайт',
            'VK',
            'FaceBook',
            'Twitter',
            'Instagram',
            'Google+',
            'Одноклассники',
        );
        return $id!=null ? $data[$id] : $data;
    }



    public function updateStatus(){
        Yii::app()->db->createCommand()
            ->update('{{master}}',array('action_time'=>date('Y-m-d H:i')),'id='.$this->id);
    }

    public static function genderList($id=null){
        $data=array(
            0=>'Мужской',
            1=>'Женский',
        );
        return $id!=null ? $data[$id] : $data;
    }

    public static function workTypeList($id=null){
        $data=array(
            0=>'Принимает в салоне',
            1=>'Выезд к клиенту',
            2=>'Принимает на дому',
        );
        return $id!=null ? $data[$id] : $data;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
