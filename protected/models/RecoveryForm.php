<?
class RecoveryForm extends Master {

    public $password;
    public $verifyPassword;
    private $_hash=null;
    public function rules(){
        return array(
            array('email','checkEmail'),
            array('password,hash','safe'),
            array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => 'Пароли не совпадают!'),
        );
    }

    public function attributeLabels(){
		return array(
			'email'=>'Электронный адрес',
            'password'=>'Пароль',
            'hash'=>'ХЕШ',
            'verifyPassword'=>'Повторите пароль'
		);
	}

    public function checkEmail(){
    	$model=Master::model()->find('email=:email',array(':email'=>trim($this->email)));
    	if (empty($model))
    		$this->addError('email','Указанный E-mail не найден!');
    	return empty($model);
    }

    public function getHash(){
        if (!$this->_hash)
    	   $this->_hash=Yii::app()->getModule('user')->encrypting($this->email);
        return $this->_hash;
    }

    public function setHash($data){
        $this->_hash=$data;
    }

    public function getLink(){
    	return Yii::app()->request->hostInfo.Yii::app()->createUrl('/master/recovery',array('hash'=>$this->getHash()));
    }

    public function sandHash(){

    	$subject="Восстановление пароля ".Yii::app()->request->hostInfo;
    	$message="Для востановления пароля перейдите по ссылке ".$this->getLink();
    	$from=Yii::app()->request->hostInfo;
    	$to=$this->email;
        return SiteHelper::sendMail($subject,$message,$to,'http://myfaceLook.ru');
    }

    public function saveHash(){
    	return Yii::app()->db->createCommand()
    		->update('{{master}}',array('hash'=>$this->getHash()),'email=:email',array(':email'=>$this->email));

    }

    public function updatePassword(){
        
        if ($this->password)
        {
            $password=Yii::app()->getModule('user')->encrypting($this->password);
            $hash=$this->getHash();
            Yii::app()->db->createCommand()->update('{{master}}',array('password'=>$password,'hash'=>''),'email=:email',array(':email'=>$this->email));
            return true;
        }

        return false;
    }

}
?>