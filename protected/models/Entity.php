<?php

/**
* This is the model class for table "{{entity}}".
*
* The followings are the available columns in table '{{entity}}':
    * @property integer $id
    * @property string $name
    * @property string $alias
*/
class Entity extends EActiveRecord
{
    public function tableName()
    {
        return '{{entity}}';
    }


    public function rules()
    {
        return array(
            array('name, alias', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, alias', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
        );
    }

    public function getUrl(){
        $url=array('master'=>'/catalog');
        $lower=$url[strtolower($this->alias)];
        return ( $lower ? $lower : '/'.$this->alias );
    }

    public function behaviors(){
        return array(
            'modelIdentifyer'=>array(
                'class'=>'application.behaviors.ModelIdentifyerBehavior',
                'attrViewName'=>'name',
                'enableSeo'=>true,
            ),
        );
    }


    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
