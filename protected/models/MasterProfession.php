<?php

/**
* This is the model class for table "{{master_profession}}".
*
* The followings are the available columns in table '{{master_profession}}':
    * @property integer $id
    * @property integer $id_master
    * @property integer $id_profession
*/
class MasterProfession extends EActiveRecord
{
    public function tableName()
    {
        return '{{master_profession}}';
    }


    public function rules()
    {
        return array(
            array('id_master, id_profession', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_master, id_profession', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master' => 'Мастер',
            'id_profession' => 'Профессия',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master',$this->id_master);
		$criteria->compare('id_profession',$this->id_profession);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
