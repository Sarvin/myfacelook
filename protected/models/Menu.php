<?php

/**
* This is the model class for table "{{menu}}".
*
* The followings are the available columns in table '{{menu}}':
    * @property integer $id
    * @property integer $name
    * @property integer $desc
    * @property integer $display
*/
class Menu extends EActiveRecord
{
    
    private $_items;
    public $outerItems;

    public function getItems(){
        if (!$this->_items){
            $this->_items=CHtml::listData($this->menuItem,'id_model_ident','id_model_ident');
        }
        
        return $this->_items;
    }
    public function setItems($data){

        $this->_items=$data;
    }

    public function tableName()
    {
        return '{{menu}}';
    }

    public function getParents(){
        return MenuItem::model()->findAll('id_menu=:menu and ((parent is null) or (parent=0))',array(':menu'=>$this->id));
    }

    public function translition($id=2){
        $data=array(
            1=>'Меню',
            2=>'Меню',
            3=>'Меню',
        );
        return $data[$id];
    }

    public function render($items,$child=false){
        $content=array();

        foreach ($items as $key => $data) {
            $content[]=CHtml::openTag('li').
                            CHtml::link($data->titleView, $data->getUrl()).' '.
                            ($data->childs ? CHtml::openTag('ul').$this->render($data->childs).CHtml::closeTag('ul') : '').
                       CHtml::closeTag('li')." ";
            
        }
        return implode(" ", $content);
    }

    public function rules()
    {
        return array(
            array('name','length','max'=>255),
            array('items,outerItems','safe'),
            // The following rule is used by search().
            array('id, name, display', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'menuItem'=>array(self::HAS_MANY,'MenuItem','id_menu','order'=>'parent asc'),
            'parentItem'=>array(self::HAS_MANY,'MenuItem','id_menu','order'=>'parent asc','condition'=>'parent is null'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
        );
    }

    public function afterSave(){
        parent::afterSave();
        
        $parents=array();
        $notIn=array(0);
        if ($this->outerItems){
            foreach ($this->outerItems as $key => $data) {
                
                if (!$model=MenuItem::model()->findByPk($data['id']))
                    $model=new MenuItem;

                $model->attributes=$data;
                $model->id_menu=$this->id;
                $model->save();
                $notIn[]=$model->id;
                $parents[$data['id']]=$model->id;
            }
        }
        MenuItem::model()->deleteAll('id not in ('.implode(",",$notIn).') and id_menu=:menu',array(':menu'=>$this->id));
        
        foreach ($parents as $key => $data) {
            Yii::app()->db->createCommand()->update('{{menu_item}}',array('parent'=>$data),'parent=:p',array(':p'=>$key));
        }
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
