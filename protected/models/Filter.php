<?
class Filter extends CModel {
	public $id_area;//;
	public $id_city;//
	public $gender;//
	public $time;//
	public $price;//
	public $service;//
	public $status;//;
	public $term;
	public $work_place;
	public $search_type;
	public $search_id;
	public $redirect;
	public $meta_desc;
	public $profession;
	private $_criteria;
	public $model;
	public static function searchTypes($id=false){
		$data=array('serice'=>1,'master'=>2,'work_place'=>3);
		return $id ? $data[$id] : $data;
	}

	public function rules(){
		return array(
			array('service,id_area,date,gender,time,price,criteria,status,term,search_type,search_id,redirect','safe')
		);
	}

	public function regSeo(){

		$parentService=Yii::app()->db->createCommand()
				->select('name')
				->from('{{service}}')
				->where('parent is null or parent=0')
				->queryAll();
		$parentService=array_map(function($n){return $n['name'];}, $parentService);
		$services=implode(', ',$parentService);

		$professions=Yii::app()->db->createCommand()
				->select('name')
				->from('{{profession}}')
				->queryAll();

		$professions=array_map(function($n){return $n['name'];}, $professions);
		$professions=implode(', ',$professions);

		$city='г. '.Yii::app()->controller->city->name;

		if ($this->model){

			$meta_title=$this->model->ident->seo->meta_title;
			$meta_key_words=$this->model->ident->seo->meta_keys;
			$meta_desc=$this->model->ident->seo->meta_desc;

		} else 
		{
			if ($this->search_type==1){
				$service=Service::model()->findByPk($this->search_id);
				if (!$service->parent){

					$meta_title=$service->name.': цены, отзывы. Частные мастера '.$services.' на Myfacelook.ru';
					$meta_key_words='анкеты, отзывы, цены, лучший, мастер, '.$professions.' , '.$city;
					$meta_desc='Myafacelook представляет лучших мастеров по '.$professions.' в '.$city.', анкеты, отзывы и цены';
				} else {

					$items=CHtml::listData($service->items,'id','name');
					$items=implode(', ',$items);
					$meta_title=$service->name.' в '.$city.' - цена, отзывы. '.$service->short_desc.' на Myfacelook.ru';
					$meta_key_words=$items.' анкеты, отзывы, цены, лучший, мастер, '.$professions.', '.$city;
					$meta_desc=$items.' Myfacelook — самый большой выбор специалистов в '.$city.' с отзывами, ценами и рейтингом, бесплатный подбор мастера и запись на приём.';
				}
			}
		}
		Yii::app()->controller->title=$meta_title;

		Yii::app()->clientScript->registerMetaTag($meta_desc  , 'description', null, array('id'=>'meta_description'), 'meta_description');
        Yii::app()->clientScript->registerMetaTag($meta_key_words, 'keywords', null, array('id'=>'keywords'), 'meta_keywords');	
        $this->meta_desc=$meta_desc;
	}

	public function setCriteria($data){
		$this->_criteria=$data;
	}

	public function getCriteria(){

		if (!$this->_criteria){
			$criteria=new CDbCriteria;
			if ($this->gender){
				$criteria->params[':gender']=$this->gender;
				$criteria->addCondition('gender=:gender');
			}

			if ($this->status)
			{
				$criteria->join.=" inner join {{master_work}} mw on mw.id_master=t.id";
				$criteria->addInCondition('mw.id_work_type',$this->status);
			}

			if ($this->id_area){
				$criteria->params[':area']=$this->id_area;
				$criteria->addCondition('id_area=:area');
			}

			if ($this->time){
				$criteria->join.=' inner join {{time_table}} tt on tt.id_master=t.id';
				switch ($this->time) {
					case 1:
						$criteria->addCondition('STR_TO_DATE(time_from,"%H:%i")<=STR_TO_DATE("12:00","%H:%i") and STR_TO_DATE(time_to,"%H:%i")>=STR_TO_DATE("12:00","%H:%i")');
						break;
					case 2:
						$criteria->addCondition('STR_TO_DATE(time_from,"%H:%i")<=STR_TO_DATE("12:00","%H:%i") and STR_TO_DATE(time_to,"%H:%i")>=STR_TO_DATE("18:00","%H:%i")');
						break;
					case 3:
						$criteria->addCondition('STR_TO_DATE(time_to,"%H:%i")>STR_TO_DATE("18:00","%H:%i")');
						break;
				}
			}

			if ($this->price || $this->search_id || $this->term)
				$criteria->join.=' inner join {{master_service}} ms on ms.id_master=t.id';

			if ($this->price){

				switch ($this->price) {
					case 1:
						$criteria->addCondition('ms.price<=500');
						break;
					case 2:
						$criteria->addCondition('ms.price>=501 and price<1001');
						break;
					case 3:
						$criteria->addCondition('ms.price>=1001 and price<3001');
						break;
					case 4:
						$criteria->addCondition('ms.price>3001');
						break;
				}
			}

			if ($this->term){

				$service=Service::model()->findByAttributes(array('name'=>$this->term));
				if ($service->id){
					if ($service->parent)
						$criteria->addCondition('id_service=:service');
					else {
						$criteria->join.=' inner join {{service}} s on s.id=ms.id_service';
						$criteria->addCondition('s.parent=:service');
					}
					
					$criteria->params[':service']=(int)$service->id;
					$this->redirect='catalog'.($service->parentService ? 
						'/'.$service->parentService->alias.'/'.$service->alias : 
						'/'.$service->alias);
				}

				if (!$service){
					$profession=Profession::model()->findByAttributes(array('name'=>$this->term));
					if ($profession)
					{
						$criteria->alias='t';
						$criteria->addCondition('t.id_profession=:prof');
						$criteria->params[':prof']=$profession->id;
						$this->redirect='/catalog/'.$profession->alias;
					}else {
						$criteria->join.=' inner join {{service}} s on s.id=ms.id_service';
						$criteria->addCondition('fio like(:term) or work_place like(:term) or s.name like(:term)');
						$criteria->params[':term']=$this->term;
					}
				}
				$this->search_id=$service ? $service->id : $profession->id;
				$this->search_type=$service ? 1 : 3;

			} else {

				$this->redirect='/catalog';
			}
			
			$criteria->distinct=true;
			$notIn=array();
			
			if ($this->id_city){
				
				$criteria->join=" inner join {{area}} a on a.id=t.id_area";
				$criteria->addCondition('id_city=:city');
				$criteria->params[':params']=$this->id_city;

			}

			$criteria->order="id asc";
			$this->_criteria=$criteria;
			return $this->_criteria;
		}
		return $this->_criteria;
	}

	public function saveInSession(){
		Yii::app()->session['filter']=serialize($this->attributes);
		Yii::app()->session['criteria']=serialize($this->_criteria);
	}

	public function attributeLabels(){
		return  array(
					'id_area'=>'Район',
					'date'=>'Дата',
					'gender'=>'Пол',
					'time'=>'Время',
					'price'=>'Цена',
					'service'=>'Услуги',
					'term'=>'Услуга, профессионал или салон',
					'id_city'=>'Город',
				);
	}

	public function attributeNames(){
		return array(
			'id_area',
			'id_city',
			'gender',
			'time',
			'price',
			'status',
			'term',
			'search_type',
			'search_id',
			'redirect'
		);
	}

	public function getAutoCompliteData(){

		$result=Yii::app()->db->createCommand()
			->select('name as value,concat("/catalog?Filter[service]=",id) as data, 1 as type, id')
			->from('{{service}}')
			->where('LOWER(name) like(\'%'.$this->term.'%\')')
			->queryAll();

		$result2=Yii::app()->db->createCommand()
			->select('fio as value,concat("/master/",alias) data, 2 as type, id')
			->from('{{master}}')
			->where('LOWER(fio) like(\'%'.$this->term.'%\')')
			->queryAll();

		$result3=Yii::app()->db->createCommand()
			->select('work_place as value,concat("/catalog?Filter[work_place]=",work_place) as data, 3 as type, id')
			->from('{{master}}')
			->where('LOWER(work_place) like(\'%'.$this->term.'%\')')
			->queryAll();

		$result4=Yii::app()->db->createCommand()
			->select('name as value,concat("/catalog?Filter[term]=",name) as data')
			->from('{{profession}}')
			->where('LOWER(name) like(\'%'.$this->term.'%\')')
			->queryAll();

		return CMap::mergeArray($result,$result2,$result3,$result4);
	}

	public static function init(){
		$model=new Filter;
		if (Yii::app()->session['filter']){
			$model->attributes=unserialize(Yii::app()->session['filter']);
			$model->setCriteria(unserialize(Yii::app()->session['criteria']));
		}
		return $model;
	}

	public static function destroy(){
		unset(Yii::app()->session['filter']);
		unset(Yii::app()->session['criteria']);
	}

	public static function getTimeList($id=false){
		$data=array('Любое','Утром (до 12:00)','Днем (12:00-18:00)','Вечером(после 18:00)');
		return $id ? $data[$id] : $data;
	}

	public static function getSessionCriteriatusList($id=false){
		
		$data=Master::workTypeList();
		return $id ? $data[$id] : $data;
	}

	public static function getGenderList($id=false){
		$data=array('Пол','Мужской','Женский');
		return $id ? $data[$id] : $data;
	}

	public static function getPriceList($id=false){
		$data=array(0=>'Цена',1=>'до 500 руб.', 2=>'от 501 до 1000 руб.',3=>'от 1001 до 3000 руб.',4=>'от 3001 руб.');
		return $id ? $data[$id] : $data;
	}

	public static function model(){
		return new Filter;
	}

	public static function getStatusList($id=false){
		$data=array(1=>'Принимает на дому',2=>'Принимает в салоне',3=>'Выезд к клиенту');
		return $id ? $data[$id] : $data;
	}

}
?>