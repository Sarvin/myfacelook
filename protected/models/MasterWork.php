<?php

/**
* This is the model class for table "{{master_work}}".
*
* The followings are the available columns in table '{{master_work}}':
    * @property integer $id
    * @property integer $id_master
    * @property integer $id_work_type
*/
class MasterWork extends EActiveRecord
{
    public function tableName()
    {
        return '{{master_work}}';
    }


    public function rules()
    {
        return array(
            array('id_master, id_work_type', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_master, id_work_type', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master' => 'Id Master',
            'id_work_type' => 'Id Work Type',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master',$this->id_master);
		$criteria->compare('id_work_type',$this->id_work_type);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
