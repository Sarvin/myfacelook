<?php

/**
* This is the model class for table "{{master_service}}".
*
* The followings are the available columns in table '{{master_service}}':
    * @property integer $id
    * @property integer $id_master
    * @property integer $id_service
    * @property integer $price
    * @property integer $time
    * @property integer $desc
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class MasterService extends EActiveRecord
{

    private $_name;

    public function getName(){
        return $this->_name;
    }

    public function setServiceName($data){
        $this->_name=$data;
    }

    public function tableName()
    {
        return '{{master_service}}';
    }

    public function getTimeView(){
        $hour=(int)date('H',strtotime($this->time));
        $min=date('i',strtotime($this->time));
        return ($hour!=0 ? Yii::t('test', '{n} час |{n} часа|{n} часов',$hour) : '').($min ? Yii::t('test', '{n} минута |{n} минуты|{n} минут',$min) : '');
    }

    public function rules()
    {
        return array(
            array('id_master, id_service, price, status, sort', 'numerical', 'integerOnly'=>true),
            array('create_time, update_time,serviceName,time, desc,', 'safe'),
            // The following rule is used by search().
            array('id, id_master, id_service, price, time, desc, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'service'=>array(self::BELONGS_TO,'Service','id_service'),
            'master'=>array(self::BELONGS_TO,'Master','id_master'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master' => 'Мастер',
            'id_service' => 'Услуга',
            'price' => 'Цена',
            'time' => 'Длительность',
            'desc' => 'Описание',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master',$this->id_master);
		$criteria->compare('id_service',$this->id_service);
		$criteria->compare('price',$this->price);
		$criteria->compare('time',$this->time);
		$criteria->compare('desc',$this->desc);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
