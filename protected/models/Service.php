<?php

class Service extends EActiveRecord
{
    public function tableName()
    {
        return '{{service}}';
    }

    public function rules()
    {
        return array(
            array('parent, status, sort', 'numerical', 'integerOnly'=>true),
            array('name,alias,short_desc', 'length', 'max'=>255),
            array('name','unique'),
            array('create_time, update_time, wswg_body', 'safe'),
            array('name', 'required'),
            // The following rule is used by search().
            array('id, name, parent, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'items'=>array(self::HAS_MANY,'Service','parent'),
            'parentService'=>array(self::BELONGS_TO,'Service','parent')
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'parent' => 'Относится к услуге:',
            'wswg_body' => 'Описание',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'short_desc' => 'Краткое описание(для сео)',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }

    public function getUrl(){
        return '/catalog'.($this->parentService ? '/'.$this->parentService->alias.'/'.$this->alias : '/'.$this->alias);
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
            'modelIdentifyer'=>array(
                'class'=>'application.behaviors.ModelIdentifyerBehavior',
                'attrViewName'=>'name',
                'enableSeo'=>true,
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function beforeSave(){
        
        if (parent::beforeSave())
        {
            $this->parent=$this->parent==null ? 0 : $this->parent;
            return true;
        }
        return false;
    }

    public static function getList($id=null){

        $parent=self::model()->findAll('(parent is null) or parent=0');
        $result=array();

        foreach ($parent as $key => $data) {
            $result[$data->name]=CHtml::listData($data->items,'id','name');
        }
        return $result;
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
