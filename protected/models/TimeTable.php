<?php

/**
* This is the model class for table "{{time_table}}".
*
* The followings are the available columns in table '{{time_table}}':
    * @property integer $id
    * @property string $id_master
    * @property string $id_week_day
    * @property string $time_from
    * @property string $time_to
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class TimeTable extends EActiveRecord
{
    public function tableName()
    {
        return '{{time_table}}';
    }


    public function rules()
    {
        return array(
            array('status, sort', 'numerical', 'integerOnly'=>true),
            array('id_master, id_week_day, time_from, time_to', 'length', 'max'=>255),
            array('create_time, update_time', 'safe'),
            array('time_from,time_to','required'),
            // The following rule is used by search().
            array('id, id_master, id_week_day, time_from, time_to, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_master' => 'Мастер',
            'id_week_day' => 'День недели',
            'time_from' => 'От',
            'time_to' => 'До',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_master',$this->id_master,true);
		$criteria->compare('id_week_day',$this->id_week_day,true);
		$criteria->compare('time_from',$this->time_from,true);
		$criteria->compare('time_to',$this->time_to,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
