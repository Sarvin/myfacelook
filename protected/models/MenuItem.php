<?php

/**
* This is the model class for table "{{menu_item}}".
*
* The followings are the available columns in table '{{menu_item}}':
    * @property integer $id
    * @property string $name
    * @property integer $parent
    * @property integer $id_menu
    * @property integer $id_model_ident
*/
class MenuItem extends EActiveRecord
{
    
    public $menuModel;

    public function getIdentName(){
        return $this->ident->instance->name;
    }

    public function getEntityName(){
        return $this->ident->entity->name;
    }

    public function afterFind(){
        parent::afterFind();
        if ($this->ident)
            $this->menuModel=$this->ident->getInstance();
    }

    public function tableName()
    {
        return '{{menu_item}}';
    }

    public function rules()
    {
        return array(
            array('parent, id_menu, id_model_ident, sort, parent', 'numerical', 'integerOnly'=>true),
            array('name, href', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, parent, id_menu, id_model_ident', 'safe', 'on'=>'search'),
        );
    }

    public function getTitleView(){
        return $this->menuModel->titleView;
    }

    public function getUrl(){
        return $this->menuModel ? strtolower($this->menuModel->getUrl()) : strtolower($this->href);
    }

    public function relations()
    {
        return array(
            'ident'=>array(self::BELONGS_TO,'ModelIdent','id_model_ident'),
            'childs'=>array(self::HAS_MANY,'MenuItem','parent'),
        );
    }



    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название элемента',
            'parent' => 'Родительский элемент',
            'id_menu' => 'Меню',
            'id_model_ident' => 'Запись',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('id_model_ident',$this->id_model_ident);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
