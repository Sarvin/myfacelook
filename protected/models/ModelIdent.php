<?php

/**
* This is the model class for table "{{model_ident}}".
*
* The followings are the available columns in table '{{model_ident}}':
    * @property integer $id
    * @property integer $id_entity
    * @property integer $id_model
*/
class ModelIdent extends EActiveRecord
{
    
    public function tableName()
    {
        return '{{model_ident}}';
    }

    public function getInstance(){
        $model=$this->id_entity!=0 ? $this->entity->alias : 'Entity';
        return $model::model()->findByPk($this->id_model);
    }

    public static function getModels(){
        
        $entities=Entity::model()->findAll();
        $models=CHtml::listData($entities,'id','alias');
        $models[0]='Entity';

        $result=Yii::app()->db->createCommand()
            ->select('id_entity,id_model')
            ->from('{{model_ident}}')
            ->queryAll();
        $return=array();

        foreach ($result as $key => $data){

            $entity=$models[$data['id_entity']];
            if ($entity){
                $model=$entity::model()->findByPk($data['id_model']);
                $return[$entity][$model->ident->id]=$model->titleView;
            }

        }
        return $return;
    }

    public function rules()
    {
        return array(
            array('id_entity, id_seo, id_model', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_entity, id_model', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'entity'=>array(self::BELONGS_TO,'Entity','id_entity'),
            'seo'=>array(self::BELONGS_TO,'Seo','id_seo'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_entity' => 'Id Entity',
            'id_model' => 'Id Model',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_entity',$this->id_entity);
		$criteria->compare('id_model',$this->id_model);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
