<?php
/**
 * Миграция m150504_092708_add_alias_to_seo
 *
 * @property string $prefix
 */
 
class m150504_092708_add_alias_to_seo extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
    public function up(){
        $this->addColumn('{{seo}}','alias','string');
    }

    public function down(){
        $this->dropColumn('{{seo}}','alias');
    }
}