<?php
/**
 * Миграция m150419_104504_master
 *
 * @property string $prefix
 */
 
class m150419_104504_master extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	private $dropped = array('{{master}}');
 
    public function safeUp()
    {
        $this->_checkTables();
 
        $this->createTable('{{master}}', array(
            'id' => 'pk', // auto increment

			'id_profession' => "int COMMENT 'Профессия'",
            'img_preview' => "text COMMENT 'Ваше фото'",
            'address' => "string COMMENT 'Адрес'",
            'gender' => "int COMMENT 'Пол'",
            'work_place' => "string COMMENT 'Место работы'",
            'wswg_body' => "text COMMENT 'Описание'",
            'links' => "text COMMENT 'Ссылки'",
            'phone' => "string COMMENT 'Телефон'",
            'email' => "string COMMENT 'Почта'",
            'balance' => "int COMMENT 'Баланс'",
            'login' => "string COMMENT 'Логин'",
            'password' => "string COMMENT 'Пароль'",
            'hash' => "string COMMENT 'Хешь'",
            'work_type' => "int COMMENT 'Тип работы'",
			'id_area' => "int COMMENT 'Район'",
            'img_background' => "text COMMENT 'Фон'",

			'status' => "tinyint COMMENT 'Статус'",
			'sort' => "integer COMMENT 'Вес для сортировки'",
            'create_time' => "datetime COMMENT 'Дата создания'",
            'update_time' => "datetime COMMENT 'Дата последнего редактирования'",
        ),
        'ENGINE=MyISAM DEFAULT CHARACTER SET = utf8 COLLATE = utf8_general_ci');
    }
 
    public function safeDown()
    {
        $this->_checkTables();
    }
 
    /**
     * Удаляет таблицы, указанные в $this->dropped из базы.
     * Наименование таблиц могут сожержать двойные фигурные скобки для указания
     * необходимости добавления префикса, например, если указано имя {{table}}
     * в действительности будет удалена таблица 'prefix_table'.
     * Префикс таблиц задается в файле конфигурации (для консоли).
     */
    private function _checkTables ()
    {
        if (empty($this->dropped)) return;
 
        $table_names = $this->getDbConnection()->getSchema()->getTableNames();
        foreach ($this->dropped as $table) {
            if (in_array($this->tableName($table), $table_names)) {
                $this->dropTable($table);
            }
        }
    }
 
    /**
     * Добавляет префикс таблицы при необходимости
     * @param $name - имя таблицы, заключенное в скобки, например {{имя}}
     * @return string
     */
    protected function tableName($name)
    {
        if($this->getDbConnection()->tablePrefix!==null && strpos($name,'{{')!==false)
            $realName=preg_replace('/{{(.*?)}}/',$this->getDbConnection()->tablePrefix.'$1',$name);
        else
            $realName=$name;
        return $realName;
    }
 
    /**
     * Получение установленного префикса таблиц базы данных
     * @return mixed
     */
    protected function getPrefix(){
        return $this->getDbConnection()->tablePrefix;
    }
}