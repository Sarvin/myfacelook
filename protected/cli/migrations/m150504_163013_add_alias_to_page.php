<?php
/**
 * Миграция m150504_163013_add_alias_to_page
 *
 * @property string $prefix
 */
 
class m150504_163013_add_alias_to_page extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{pages}}','alias','string');
    }

    public function down(){
        $this->dropColumn('{{pages}}','alias');
    }
}