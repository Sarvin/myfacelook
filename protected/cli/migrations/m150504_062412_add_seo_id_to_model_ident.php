<?php
/**
 * Миграция m150504_062412_add_seo_id_to_model_ident
 *
 * @property string $prefix
 */
 
class m150504_062412_add_seo_id_to_model_ident extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{model_ident}}','id_seo','int');
    }

    public function down(){
        $this->dropColumn('{{model_ident}}','id_seo');
    }
}