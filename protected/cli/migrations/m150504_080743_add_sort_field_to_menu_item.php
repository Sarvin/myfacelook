<?php
/**
 * Миграция m150504_080743_add_sort_field_to_menu_item
 *
 * @property string $prefix
 */
 
class m150504_080743_add_sort_field_to_menu_item extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{menu_item}}','sort','int');
    }

    public function down(){
        $this->dropColumn('{{menu_item}}','sort');
    }
}