<?php
/**
 * Миграция m150519_172625_add_shor_desc_to_service
 *
 * @property string $prefix
 */
 
class m150519_172625_add_shor_desc_to_service extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{service}}','short_desc','string');
    }

    public function down(){
        $this->dropColumn('{{service}}','short_desc');
    }

}