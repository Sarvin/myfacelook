<?php
/**
 * Миграция m150420_201726_add_like_toGallery_photo
 *
 * @property string $prefix
 */
 
class m150420_201726_add_like_toGallery_photo extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{gallery_photo}}','like','int');
    }

    public function down(){
        $this->dropColumn('{{gallery_photo}}','like');
    }
}