<?php
/**
 * Миграция m150420_195320_add_gallery_to_master
 *
 * @property string $prefix
 */
 
class m150420_195320_add_gallery_to_master extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{master}}','id_gallery','int');
    }

    public function down(){
        $this->dropColumn('{{master}}','id_gallery');
    }

}