<?php
/**
 * Миграция m150422_190921_add_fio_to_master
 *
 * @property string $prefix
 */
 
class m150422_190921_add_fio_to_master extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{master}}','fio','string');
    }

    public function down(){
        $this->dropColumn('{{master}}','fio');
    }
}