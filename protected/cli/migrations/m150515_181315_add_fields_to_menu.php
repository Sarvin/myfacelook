<?php
/**
 * Миграция m150515_181315_add_fields_to_menu
 *
 * @property string $prefix
 */
 
class m150515_181315_add_fields_to_menu extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{menu_item}}','href','string');
        $this->dropColumn('{{menu_item}}','url');
    }
    
}