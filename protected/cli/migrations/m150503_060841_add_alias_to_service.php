<?php
/**
 * Миграция m150503_060841_add_alias_to_service
 *
 * @property string $prefix
 */
 
class m150503_060841_add_alias_to_service extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{service}}','alias','string');
    }

    public function down(){
        $this->dropColumn('{{service}}','alias');
        return $this->getDbConnection()->tablePrefix;
    }
}