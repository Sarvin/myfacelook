<?php
/**
 * Миграция m150512_172015_add_wswg_body_to_service
 *
 * @property string $prefix
 */
 
class m150512_172015_add_wswg_body_to_service extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{service}}','wswg_body','text');
    }

    public function down(){
        $this->dropColumn('{{service}}','wswg_body');
    }
}