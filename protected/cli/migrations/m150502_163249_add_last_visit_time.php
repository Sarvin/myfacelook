<?php
/**
 * Миграция m150502_163249_add_last_visit_time
 *
 * @property string $prefix
 */
 
class m150502_163249_add_last_visit_time extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{master}}','action_time','string');
    }

    public function down(){
        $this->dropColumn('{{master}}','action_time');
        return $this->getDbConnection()->tablePrefix;
    }
}