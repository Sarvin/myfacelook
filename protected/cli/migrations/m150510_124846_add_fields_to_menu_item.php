<?php
/**
 * Миграция m150510_124846_add_fields_to_menu_item
 *
 * @property string $prefix
 */
 
class m150510_124846_add_fields_to_menu_item extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{menu_item}}','url','string');
    }

    public function down(){
        $this->dropColumn('{{menu_item}}','url');
    }
}