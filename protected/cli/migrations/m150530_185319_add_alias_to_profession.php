<?php
/**
 * Миграция m150530_185319_add_alias_to_profession
 *
 * @property string $prefix
 */
 
class m150530_185319_add_alias_to_profession extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function up(){
        $this->addColumn('{{profession}}','alias','string');
    }

    public function down(){
        $this->dropColumn('{{profession}}','alias');
    }
}