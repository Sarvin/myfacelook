<?php
/**
 * Миграция m150530_190419_add_wswg_body_to_profession
 *
 * @property string $prefix
 */
 
class m150530_190419_add_wswg_body_to_profession extends CDbMigration
{
    public function up(){
        $this->addColumn('{{profession}}','wswg_body','text');
    }

    public function down(){
        $this->dropColumn('{{profession}}','wswg_body');
    }
}