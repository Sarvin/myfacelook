<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string page name.
     */
    public $title;
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/simple';
    public $regSeo=true;
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    //for link in main menu
    public $action = null;

    public $cs;

    protected function preinit()
    {
        parent::preinit();
    }

    private $_model=false;

    public function getModel(){
        return $this->_model;
    }

    public $city;

    public function getCityName(){
        $api_key='APGXNU8BAAAAOjeGfAIAy6i9Wm0oT4xtrf2sJyvxEx4daqwAAAAAAAAAAAAUbLV4HKCVZeT5UPJKAxlP4-tjRA==';
        $arr=array(
            'common'=>array(
                'version'=>'1.0',
                'api_key'=>$api_key
            ),
            'ip'=>array(
                'address_v4'=>$_SERVER['REMOTE_ADDR'],
                //'address_v4'=>"188.186.0.18"
            ),
        );
        $json=json_encode($arr);
        $addr="http://api.lbs.yandex.net/geolocation?json=".$json;
        
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_URL, $addr);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0');
        $res = curl_exec($curl);
        if($res)
        {
            $result=json_decode($res);
        }
        curl_close($curl);
        if ($result->position->latitude && $result->position->longitude){
           $request=file_get_contents('http://geocode-maps.yandex.ru/1.x/?results=1&key='.$api_key.'&format=json&kind=locality&geocode='.$result->position->longitude.','.$result->position->latitude);
           $response=json_decode($request);
           $cityname=$response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->LocalityName;
        }
        return $cityname;
    }

    public function setModel($data){
        if ($data)
        {
            if ($data->hasAttribute('status')){
                if ($data->status!=1 && $data->status!=4)
                    throw new CHttpException(404,'Страница не найдена');
            }
            $this->_model=$data;
            if ($data->ident)
            {
                if ($data->ident->metaData->hasRelation('seo') && $this->regSeo)
                {
                    $this->registerSeo($data->ident->seo);
                }
            }
        } else 
            throw new CHttpException(404,'Страница не найдена');

    }

    public function registerSeo($seo){

            if ( !empty($seo->meta_title) )
                $this->title = $seo->meta_title;
            else
                $this->title = $this->_model->titleView.' | '.Yii::app()->config->get('app.name');
            
            if ($seo->meta_desc)
                Yii::app()->clientScript->registerMetaTag($seo->meta_desc  , 'description', null, array('id'=>'meta_description'), 'meta_description');

            Yii::app()->clientScript->registerMetaTag($seo->meta_keys, 'keywords', null, array('id'=>'keywords'), 'meta_keywords');
    }

    public function init(){
        parent::init();

        $this->title = Yii::app()->name;
        $this->cs = Yii::app()->clientScript;
        $this->cs->registerCoreScript('jquery');
        $city=$this->getCityName();
        
        if (!$city)
            $city='Тюмень';
        $this->city=City::model()->findByAttributes(array('name'=>$city));
        if(Yii::app()->getRequest()->getParam('update_assets')) $this->forceCopyAssets = true;
    }

    //Get Clip
    public function getClip($name){
        if (isset($this->clips[$name])) return $this->clips[$name];
        return '';
    }

    //Check home page
    public function is_home(){
        return $this->route == 'site/index';
    }

	protected $assetsUrl;
	protected $assetsMap = array();
	protected $forceCopyAssets = true;
	public function getAssetsUrl($moduleName = false)
	{
		if ( $moduleName ) {
			if ( !isset($this->assetsMap[$moduleName]) ) {
				if ( $moduleName === 'application' and isset(Yii::app()->theme) ) {
					$assetsPath = Yii::app()->theme->getBasePath().DIRECTORY_SEPARATOR.'assets';
				} else {
					$assetsPath = Yii::getPathOfAlias($moduleName.'.assets');
				}
				$this->assetsMap[$moduleName] = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);
			}
			return $this->assetsMap[$moduleName];
		}

		if ( !isset($this->assetsUrl) )
		{
			if ( $this->module ) {
				$assetsPath = Yii::getPathOfAlias($this->module->name.'.assets');
			} else if ( isset(Yii::app()->theme) ) {
				$assetsPath = Yii::app()->theme->getBasePath().DIRECTORY_SEPARATOR.'assets';
			} else {
				$assetsPath = Yii::getPathOfAlias('application.assets');
			}
			$this->assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);
		}
		return $this->assetsUrl;
	}

    public function beforeRender($view)
    {
        return parent::beforeRender($view);
    }

    /**
     * Loads the requested data model.
     * @param string the model class name
     * @param integer the model ID
     * @param array additional search criteria
     * @param boolean whether to throw exception if the model is not found. Defaults to true.
     * @return CActiveRecord the model instance.
     * @throws CHttpException if the model cannot be found
     */
    protected function loadModel($class, $id, $criteria = array(), $exceptionOnNull = true)
    {
        if (empty($criteria)) {
            $model = CActiveRecord::model($class)->findByPk($id);
        } else {
            $finder = CActiveRecord::model($class);
            $c = new CDbCriteria($criteria);
            $c->mergeWith(array(
                'condition' => $finder->tableSchema->primaryKey . '=:id',
                'params' => array(':id' => $id),
            ));
            $model = $finder->find($c);
        }
        if (isset($model))
            return $model;
        else if ($exceptionOnNull)
            throw new CHttpException(404, 'Unable to find the requested object.');
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $model->formId)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Outputs (echo) json representation of $data, prints html on debug mode.
     * NOTE: json_encode exists in PHP > 5.2, so it's safe to use it directly without checking
     * @param array $data the data (PHP array) to be encoded into json array
     * @param int $opts Bitmask consisting of JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS, JSON_FORCE_OBJECT.
     */
    public function renderJson($data, $opts=null)
    {
        if(YII_DEBUG && isset($_GET['debug']) && is_array($data))
        {
            foreach($data as $type => $v)
                printf('<h1>%s</h1>%s', $type, is_array($v) ? json_encode($v, $opts) : $v);
        }
        else
        {
            header('Content-Type: application/json; charset=UTF-8');
            echo json_encode($data, $opts);
        }
    }

    /**
     * Utility function to ensure the base url.
     * @param $url
     * @return string
     */
    public function baseUrl( $url = '' )
    {
        static $baseUrl;
        if ($baseUrl === null)
            $baseUrl = Yii::app()->request->baseUrl;
        return $baseUrl . '/' . ltrim($url, '/');
    }


	protected function disableLogRoutes()
	{
		foreach (Yii::app()->log->routes as $route)
		{
			if ($route instanceof CLogRoute)
			{
				$route->enabled = false;
			}
		}
	}
}