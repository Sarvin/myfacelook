<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class FrontController extends Controller
{
    public $layout='//layouts/simple';
    public $menu=array();
    public $breadcrumbs=array();
    public $bg=false;
    public function init() {
        parent::init();
        // $session=new CHttpSession;
        // $session->open();
        $this->title = Yii::app()->name;
    }

    //Check home page
    public function is_home(){
        return $this->route == 'site/index';
    }

    public function beforeAction($action){
        parent::beforeAction($action);
        if ($this->id=='site')
            $this->bg=true;
        
        // $session=new CHttpSession;
        // $session->open();

        if (strtolower(Yii::app()->controller->id.$action->id)!='catalogindex'){
            Filter::destroy();
        }
        return true;
    }

    public function beforeRender($view)
    {
        return parent::beforeRender($view);
    }

}