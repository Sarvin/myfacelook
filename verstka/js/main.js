$(function(){
	
	
	
	if ($('.date').length)
	{
		$('.date input').datepicker();
	}
	if ($('#calendar').length)
	{
		$('.wrap .dt').datepicker();
	}
	if ($('.carusel').length){
		$('.carusel').owlCarousel({
			items:1,
			singleItem:true,
			padding:0,
			margin:0,
			nav:true,
			navText:[],
			loop:true,
		})
	}
	$('select').selectBox();
	if ($('#scrollbarY').length)
		$('#scrollbarY,#calendar,.services').tinyscrollbar();
	if ($('.area').length){
		$('.area').autocomplete({
		});
	}
})
